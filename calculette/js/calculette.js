'use strict'


//recup de mes touches
var touches = document.querySelectorAll('th');

//un tableau d'op pour diff des autres touches (nombres) je stoke dans une var car y en a plusieurs
var operateurs = ['/', 'x', '-', '+'];

//on va bouclé avec for car
//addeventlister ne marche pas
for(var i = 0; i < touches.length; i++) {
	touches[i].onclick = function(toto) {

		/*obtenir les nombres à écran (pour le moment)
		innerHTML= permet d'afficher
		*/
		var ecran = document.querySelector('.ecran');
		var valeurEcran = ecran.innerHTML;
		var valeurBouton = this.innerHTML;
		//this permet de select les element au moment meme ou on clique

		//CALCUL ET DIFF DES TOUCHES (AC/=/./var operateurs)

		// Touche "reset"
		if(valeurBouton == 'AC') {

			// Reset écran et reset flag decimale.
			ecran.innerHTML = '';

		// Touche "=".
		} else	if(valeurBouton == '=') {

			// on renvois la var calcul à l'écran
			var calcul = valeurEcran;

			// obtention du dernier caractère
			var dernierCaractere = valeurEcran[valeurEcran.length - 1];

			// dans notre var de calcul remplacement de x par *
			calcul = calcul.replace(/x/g, '*');

			// si le calcul se termine par un . ou un operateur on enlève cet opérateur
			if(operateurs.indexOf(dernierCaractere) > -1
			|| dernierCaractere == '.') {
				calcul = calcul.replace(/.$/, '');
			}
			// si il y a un calcul, utilisation de eval pour que l'écran change de valeur

			if(calcul) {
				ecran.innerHTML = eval(calcul);
			}


		} else if (operateurs.indexOf(valeurBouton) > -1) {

			/*
			 - il reste quelques problèmes :
			 - 1 Le calcul avec deux opérateurs successifs doivent être interdits
			 - 2 Le calcul ne peut pas commencer par un opérateur, mis à part un -
			 - 3 Le calcul avec plus d'une décimale doit être interdit.
			 */

			// obtention du dernier caractère de l'écran.
			var dernierCaractere = valeurEcran[valeurEcran.length - 1];

			// ajout de l'opérateur seulement si l'écran n'a pas d'opérateur comme dernier caractère.

			// reset l'écran si l'écran vaut "-" et qu'on presse "+".
			if (valeurEcran == '-'&& valeurBouton == '+')
			{
				ecran.innerHTML = '';
			}
			// ajout - si l'écran est vide et qu'on presse -
			else if (valeurEcran == '' && valeurBouton == '-')
			{
				ecran.innerHTML = valeurBouton;
			}
			// si il y a quelque chose dans l'écran et que ce n'est pas -
			else if (valeurEcran != '' && operateurs.indexOf(dernierCaractere) == -1)
			{
				ecran.innerHTML += valeurBouton;
			}
		}
		else
		{
			// si c'est une autre touche que égal, ajout à la suite de l'écran.
			ecran.innerHTML += valeurBouton;
		}
	}
}
//je n'ai pas réglé le problèmes de la décimale :^(
