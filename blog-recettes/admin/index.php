<?php
// Afficher toutes les erreurs : à commenter en production
ini_set('display_errors', 1);
error_reporting(E_ALL);

include_once('../config/connect.php');
require_once '../inc/markdown/Markdown.inc.php';
use Michelf\Markdown;

function markToHtml( $content )
{
  echo Markdown::defaultTransform(htmlspecialchars( $content ));
}


$contexte = 'index';
$title= 'Admin CMS Mini';


/*
    REQUETE RUBRIQUES
    Récupérer toutes les rubriques

*/

$query = $pdo->prepare('SELECT * FROM rubriques');
$query->execute(); //Exécution de la requête
$rubriques = $query->fetchAll(); //Résultats stockées dans $rubriques : un tableau dont chaque ligne contient 1 rubrique;
$query->closeCursor();//Fermeture de la requête (pour libérer les ressources de la base de données associées à $query)


/*
    REQUETE MEDIAS
    Récupérer les médias

*/

$query = $pdo->prepare('SELECT * FROM medias');
$query->execute(); //Exécution de la requête
$medias = $query->fetchAll(); //Résultats stockées dans $medias : un tableau dont chaque ligne contient 1 rubrique;
$query->closeCursor();//Fermeture de la requête (pour libérer les ressources de la base de données associées à $query)

/*
    REQUETE TAGS
    Récupérer les TAGS

*/

$query = $pdo->prepare('SELECT * FROM tags');
$query->execute(); //Exécution de la requête
$tags = $query->fetchAll(); //Résultats stockées dans $tags : un tableau dont chaque ligne contient 1 rubrique;
$query->closeCursor();//Fermeture de la requête (pour libérer les ressources de la base de données associées à $query)


/*
    FORMULAIRE DE SUPPRESSION D'UN ARTICLE
    Requête de mise à jour des tables
      articles
      medias_liaisons
*/

if( array_key_exists('delete_article', $_POST) )
{
  $id = intval($_POST['id']);

  $queryLiaison = $pdo->prepare("
    DELETE FROM medias_liaisons
    WHERE id_article = ?
  ");

  $queryLiaison->execute( array($id) );
  $queryLiaison->closeCursor();

  $query = $pdo->prepare("
    DELETE FROM articles
    WHERE id = ?
  ");

  $query->execute( array($id) );
  $query->closeCursor();

  header('Location:'.BLOG_URL . '/admin/index.php');

}

/*
    FORMULAIRE DE SUPPRESSION D'UN MEDIA
    Requête de mise à jour des tables
      medias
      medias_liaisons
*/

if( array_key_exists('delete_media', $_POST) )
{
  $id_media = intval($_POST['id_media']);
  $file = $_POST['file'];

  $queryLiaison = $pdo->prepare("
    DELETE FROM medias_liaisons
    WHERE id_media = ?
  ");

  $queryLiaison->execute( array($id_media) );
  $queryLiaison->closeCursor();

  $query = $pdo->prepare("
    DELETE FROM medias
    WHERE id = ?
  ");

  $query->execute( array($id_media) );
  $query->closeCursor();

  unlink( '../medias/' . $file);

  header('Location:'.BLOG_URL . '/admin/index.php');

}

/*
    FORMULAIRE DE SUPPRESSION D'UN TAG
    Requête de mise à jour des tables
      tags
      tags_liaisons
*/

if( array_key_exists('delete_tag', $_POST) )
{
  $id_tag = intval($_POST['id']);

  $queryLiaison = $pdo->prepare("
    DELETE FROM tags_liaisons
    WHERE id_tag = ?
  ");

  $queryLiaison->execute( array($id_tag) );
  $queryLiaison->closeCursor();

  $query = $pdo->prepare("
    DELETE FROM tags
    WHERE id = ?
  ");

  $query->execute( array($id_tag) );
  $query->closeCursor();

  header('Location:'.BLOG_URL . '/admin/index.php');

}





/*
    REQUETE ARTICLES
    Récupérer les articles classés par date inversée
*/

/*
  On exécute directement la requête
  puis on stocke le résultat dans $count
  ->fetchColumn() renvoie directement la valeur de la première ligne
*/
$query = $pdo->query('SELECT COUNT(*) FROM articles');
$count = $query->fetchColumn();
$query->closeCursor();

//$query = $pdo->prepare('SELECT * FROM articles ORDER BY date DESC LIMIT 10');

$query = $pdo->prepare("
  SELECT
  articles.id AS id,
  articles.titre AS titre,
  rubriques.titre AS rub_titre,
	DATE_FORMAT(date, '%d/%m/%Y') AS date
  FROM articles
  INNER JOIN rubriques ON articles.id_rub = rubriques.id
  ORDER BY date DESC
");

$query->execute();

//$articles = $query->fetchAll();
//$query->closeCursor();

// Inclure la vue
include './templates/' . $contexte. 'View.php';
