<?php
// Afficher toutes les erreurs : à commenter en production
ini_set('display_errors', 1);
error_reporting(E_ALL);

include_once('../config/connect.php');


$contexte = 'newRubrique';
$title= 'Nouvelle rubrique';

if( array_key_exists('title', $_POST) )
{
  $title = $_POST['title'];
  $content = $_POST['content'];

  $query = $pdo->prepare("INSERT INTO rubriques (titre, texte) VALUES (?, ?)");
  $query->execute( array($title, $content) );

  //On récupère l'id nouvellement créée afin de rediriger sur la page de visualisation de ce nouvel article
  $id = $pdo->lastInsertId();

  // On redirige l'internaute vers la page de modification et de visualisation de la rubrique (contrôleur : updateRubrique.php)
  header('Location:'.BLOG_URL . '/admin/updateRubrique.php?id=' . $id);
  exit;
}

include './templates/' . $contexte. 'View.php';
