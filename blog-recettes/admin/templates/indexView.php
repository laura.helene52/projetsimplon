<?php include dirname(__FILE__) . '/include/header.php'; ?>

<main class="col-md-10">
    <nav class="mt-2" aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Accueil</li>
      </ol>
    </nav>

    <h1 class="mb-5 text-center h4">Accueil de l'interface d'administration</h1>
    <div class="row">
    <?php if( !empty( $rubriques) ) : ?>
      <section class="col-md-8">
        <h2>Les rubriques</h2>
        <table class="table table-striped table-bordered">
          <caption>Liste des rubriques (<?php echo count($rubriques) ?> rubriques au total)</caption>
          <thead class="thead-dark">
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Titre</th>
              <th scope="col">Texte</th>
            </tr>
          </thead>
          <tbody>
        <?php foreach( $rubriques as $rubrique ) : ?>
            <tr>
              <th scope="row"><?php echo $rubrique['id'] ?></th>
              <td><a href="updateRubrique.php?id=<?php echo $rubrique['id'] ?>" title="Modifier la rubrique"><?php echo $rubrique['titre'] ?></a></td>
              <td><?php echo $rubrique['texte'] ?></td>
            </tr>
        <?php endforeach; ?>
          <tbody>
        </table>
      </section>
    <?php endif; ?>
    <?php if( !empty( $tags) ) : ?>
      <section class="col-md-4">
        <h2>Les tags</h2>
        <table class="table table-striped table-bordered">
          <caption>Liste des tags (<?php echo count($tags) ?> tags au total)</caption>
          <thead class="thead-dark">
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Titre</th>
              <th scope="col">Supprimer</th>
            </tr>
          </thead>
          <tbody>
        <?php foreach( $tags as $tag ) : ?>
            <tr>
              <th scope="row"><?php echo $tag['id'] ?></th>
              <td><a href="updateTag.php?id=<?php echo $tag['id'] ?>" title="Modifier la rubrique"><?php echo strip_tags( $tag['titre'] ) ?></a></td>
              <td>
                <form method="post" class="w-50">
                  <div class="form-check pr-5 align-self-center">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      name="id"
                      value="<?php echo $tag['id'] ?>"
                      aria-label ="Cocher cette case pour supprimer le tag"
                    >
                      <input type="submit" name="delete_tag" value="Supprimer le tag"  class="btn btn-sm btn-primary">
                  </div>
                </form>
              </td>
            </tr>
        <?php endforeach; ?>
          <tbody>
        </table>
      </section>
    <?php endif; ?>
  </div>
  <div class="row mt-5">
    <?php if( $count > 0 ) : ?>
      <section class="col-md-8">
        <h2>Les articles</h2>
        <table class="table table-striped table-bordered">
          <caption>Total : <?php echo $count ?> articles</caption>
          <thead class="thead-dark">
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Date</th>
              <th scope="col">Titre</th>
              <th scope="col">Rubrique</th>
              <th scope="col">Supprimer l'article</th>
            </tr>
          </thead>
          <tbody>
        <?php while( $article = $query->fetch() ) : ?>
            <tr>
              <th scope="row"><?php echo $article['id'] ?></th>
              <td class="small"><?php echo $article['date'] ?></td>
              <td><a href="updateArticle.php?id=<?php echo $article['id'] ?>" title="Modifier l'article"><?php echo strip_tags( $article['titre'] ) ?></a></td>
              <td><?php echo strip_tags( $article['rub_titre'] ) ?></td>
              <td>
                <form method="post" class="w-50">
                  <div class="form-check pr-5 align-self-center">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      name="id"
                      value="<?php echo $article['id'] ?>"
                      aria-label ="Cocher cette case pour supprimer l'article"
                    >
                      <input type="submit" name="delete_article" value="Supprimer l'article"  class="btn btn-sm btn-primary">
                  </div>
                </form>
              </td>
            </tr>
        <?php endwhile; ?>
          <tbody>
        </table>

      </section>
    <?php endif; ?>

    <?php if( !empty( $medias) ) : ?>
      <section class="col-md-4">
        <h2>Les medias</h2>
        <table class="table table-striped table-bordered table-sm">
          <caption>Total : <?php echo $count ?> médias</caption>
          <thead class="thead-dark">
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Medias</th>
              <th scope="col">Supprimer le média</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach( $medias as $media ) : ?>
            <tr>
              <th scope="row"><?php echo $media['id'] ?></th>
              <td>
                <a class="d-inline-block text-truncate" style="max-width: 150px;" href="updateMedia.php?id=<?php echo $media['id'] ?>" title="Modifier le media">
                  <?php echo strip_tags( $media['file'] ) ?><br>
                  <img height="100" src="../medias/<?php echo strip_tags( $media['file'] ) ?>" alt="">
                </a>
              </td>
              <td>
                <form method="post" class="w-50">
                  <input type="hidden" name="file" value="<?php echo strip_tags( $media['file'] ) ?>" >
                  <div class="form-check pr-5 align-self-center">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      name="id_media"
                      value="<?php echo $media['id'] ?>"
                      aria-label ="Cocher cette case pour supprimer le média"
                    >
                      <input type="submit" name="delete_media" value="Supprimer"  class="btn btn-sm btn-primary">
                  </div>
                </form>
              </td>
            </tr>
          <?php endforeach; ?>
          <tbody>
        </table>
      </section>
    <?php endif; ?>
  </div>
</main>


<?php include dirname(__FILE__) . '/include/footer.php'; ?>
