<?php include dirname(__FILE__) . '/include/header.php';?>
<main class="col-md-10">
  <nav class="mt-2" aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo BLOG_URL ?>/admin/">Accueil</a></li>
      <li class="breadcrumb-item active" aria-current="page">Nouvelle article</li>
    </ol>
  </nav>

  <form method="post">
    <fieldset>
      <legend>Nouvel article</legend>
      <div class="form-group">
        <label for="rub">Dans la rubrique</label>
        <select class="form-control" id="rub" name="id_rub">
          <?php foreach($rubriques as $rubrique) : ?>
          <option value="<?php echo $rubrique['id']?>"><?php echo $rubrique['titre']?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="form-group">
        <label for="title">Titre</label>
        <input type="text" class="form-control" id="title" name="title" required>
      </div>
      <div class="form-group">
        <label for="content">Contenu</label>
        <textarea class="form-control" id="content" name="content"></textarea>
      </div>
    </fieldset>
    <button type="submit" class="btn btn-primary">Insérer un nouvel article</button>
  </form>

</main>

<?php include dirname(__FILE__) . '/include/footer.php'; ?>
