<?php include dirname(__FILE__) . '/include/header.php';?>
<main class="col-md-10">
  <nav class="mt-2" aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo BLOG_URL ?>/admin/">Accueil</a></li>
      <li class="breadcrumb-item active" aria-current="page">Nouveau média</li>
    </ol>
  </nav>

  <form method="post" enctype="multipart/form-data">
    <input type="hidden" name="MAX_FILE_SIZE" value="2000000">
    <fieldset>
      <legend>Nouveau média</legend>
      <?php if( !empty( $error) ) : ?>
        <p class="bg-dark text-white border border-danger p-2">
          <?php echo $error; ?>
        </p>
      <?php endif; ?>
      <div class="form-group">
        <label for="file">Choisir un fichier sur son poste</label>
        <input type="file" class="form-control" id="file" name="file">
      </div>
      <div class="form-group">
        <label for="content">Légende ou alternative textuelle</label>
        <textarea class="form-control" id="content" name="legend"></textarea>
      </div>
    </fieldset>
    <button type="submit" class="btn btn-primary">Nouveau média</button>
  </form>

</main>

<?php include dirname(__FILE__) . '/include/footer.php'; ?>
