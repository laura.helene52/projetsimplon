<?php include dirname(__FILE__) . '/include/header.php'; ?>
<main class="col-md-10">
  <nav class="mt-2" aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo BLOG_URL ?>/admin/">Accueil</a></li>
      <li class="breadcrumb-item active" aria-current="page"><?php echo strip_tags( $tag['titre'] ) ?></li>
    </ol>
  </nav>
  <div class="row">
    <div class="col-md-6" role="form">
      <form method="post">
        <input type="hidden" name="id" value="<?php echo $tag['id'] ?>">
        <fieldset>
          <legend>Modifier le tag</legend>
          <div class="form-group">
            <label for="title">Titre</label>
            <input type="text" class="form-control" id="title" name="title" value="<?php echo strip_tags( $tag['titre'] ) ?>" required>
          </div>
        </fieldset>
        <button type="submit" class="btn btn-primary">Modifier le tag</button>
      </form>
    </div>
    <div class="col-md-6">
      <?php if( !empty( $articles) ) : ?>
      <h2 class="mt-3 h4">Articles liés</h2>
      <table class="table table-striped table-bordered">
        <caption>Total : <?php echo count($articles) ?> articles</caption>
        <thead class="thead-dark">
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Date</th>
            <th scope="col">Titre</th>
          </tr>
        </thead>
        <tbody>
      <?php foreach( $articles as $article ) : ?>
          <tr>
            <td scope="row"><?php echo $article['id'] ?></td>
            <td class="small"><?php echo $article['date'] ?></td>
            <td><a href="updateArticle.php?id=<?php echo $article['id'] ?>" title="Modifier l'article"><?php echo $article['titre'] ?></a></td>
          </tr>
      <?php endforeach; ?>
        <tbody>
      </table>
    <?php endif; ?>
    </div>
  </div>
</main>

<?php include dirname(__FILE__) . '/include/footer.php'; ?>
