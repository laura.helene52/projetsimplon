<?php include dirname(__FILE__) . '/include/header.php';?>
<main class="col-md-10">
  <nav class="mt-2" aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo BLOG_URL ?>/admin/">Accueil</a></li>
      <li class="breadcrumb-item active" aria-current="page">Nouveau tag</li>
    </ol>
  </nav>

  <form method="post">
    <fieldset>
      <legend>Nouveau tag</legend>
      <div class="form-group">
        <label for="title">Titre</label>
        <input type="text" class="form-control" id="title" name="title" required>
      </div>
    </fieldset>
    <button type="submit" class="btn btn-primary">Insérer un nouveau tag</button>
  </form>

</main>

<?php include dirname(__FILE__) . '/include/footer.php'; ?>
