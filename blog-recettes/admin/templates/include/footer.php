    </div><!-- fin .row -->
  </div><!-- fin .container -->

  <script src="<?php echo BLOG_URL ?>/admin/templates/js/vendor/jquery-3.2.1.min.js"></script>
  <script src="<?php echo BLOG_URL ?>/admin/templates/js/bootstrap.min.js"></script>

  <script type="text/javascript" src="<?php echo BLOG_URL ?>/admin/templates/js/simplemde.min.js"></script>
  <script>
    var simplemde = new SimpleMDE({
        element: document.getElementById("content"),
        toolbar: [
            "bold", "italic", "|",
            'unordered-list', 'ordered-list', "|",
            "heading-2", "heading-3", "|",
            "quote", 'horizontal-rule', "|",
            'link', 'image', "|",
            'preview','fullscreen','side-by-side'
          ]
    });
  </script>


  </body>
</html>
