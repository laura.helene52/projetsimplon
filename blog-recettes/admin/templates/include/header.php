<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="manifest" href="site.webmanifest">
        <link rel="apple-touch-icon" href="icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="<?php echo BLOG_URL ?>/admin/templates/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo BLOG_URL ?>/admin/templates/css/simplemde.min.css" />
    </head>
    <body>
      <header class="navbar navbar-expand-lg navbar-dark bg-dark border-bottom justify-content-between">
        <a href="<?php echo BLOG_URL ?>/admin/" class="navbar-brand" title="Administration du blog">CMS MINI</a>
        <div class="navbar-nav">
          <a class="btn btn-info" href="../">Voir le site public</a>
        </div>
      </header>
      <div class="container-fluid">
        <div class="row" style="min-height:100vh;">
          <nav class="col-md-2 navbar navbar-dark bg-dark">
            <ul class="navbar-nav flex-column mb-auto">
              <li class="nav-item">
                <a class="nav-link<?php if($contexte == 'index' ) echo ' active' ?>" href="<?php echo BLOG_URL ?>/admin/">Accueil</a>
              </li>
              <li class="nav-item">
                <a class="nav-link<?php if($contexte == 'newArticle' ) echo ' active' ?>" href="<?php echo BLOG_URL ?>/admin/newArticle.php">Nouvel article</a>
              </li>
              <li class="nav-item">
                <a class="nav-link<?php if($contexte == 'newRubrique' ) echo ' active' ?>" href="<?php echo BLOG_URL ?>/admin/newRubrique.php">Nouvelle rubrique</a>
              </li>
              <li class="nav-item">
                <a class="nav-link<?php if($contexte == 'newMedia' ) echo ' active' ?>" href="<?php echo BLOG_URL ?>/admin/newMedia.php">Nouveau média</a>
              </li>
              <li class="nav-item">
                <a class="nav-link<?php if($contexte == 'newTag' ) echo ' active' ?>" href="<?php echo BLOG_URL ?>/admin/newTag.php">Nouveau Tag</a>
              </li>
            </ul>
          </nav>
