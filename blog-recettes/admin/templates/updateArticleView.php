<?php include dirname(__FILE__) . '/include/header.php'; ?>
<main class="col-md-10 p-5">
  <nav class="mt-2" aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo BLOG_URL ?>/admin/">Accueil</a></li>
        <li class="breadcrumb-item"><a href="<?php echo BLOG_URL ?>/admin/updateRubrique.php?id=<?php echo $article['id_rub'] ?>"><?php echo $article['rub_titre'] ?></a></li>
      <li class="breadcrumb-item active" aria-current="page"><?php echo strip_tags( $article['titre'] ) ?></li>
    </ol>
  </nav>
  <div class="row">
    <div class="col-md-6" role="form">
      <form method="post">
        <input type="hidden" name="id" value="<?php echo $article['id'] ?>">
        <fieldset>
          <legend>Modifier l'article</legend>
          <div class="form-group">
            <label for="rub">Dans la rubrique</label>
            <select class="form-control" id="rub" name="id_rub">
              <?php foreach($rubriques as $rubrique) : ?>
              <option
                value="<?php echo $rubrique['id']?>"
                <?php if( $rubrique['id'] == $article['id_rub'] ) echo ' selected' ?>
              >
                <?php echo $rubrique['titre']?>
              </option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="form-group">
            <label for="title">Titre</label>
            <input type="text" class="form-control" id="title" name="title" value="<?php echo strip_tags( $article['titre'] ) ?>" required>
          </div>
          <div class="form-group">
            <label for="content">Contenu</label>
            <textarea class="form-control" id="content" name="content"><?php echo strip_tags( $article['texte'] ) ?></textarea>
          </div>
        </fieldset>
        <button type="submit" class="btn btn-primary">Modifier l'article</button>
      </form>
    </div>
    <div class="col-md-6">
      <h2 class="mb-3 h4">Données de l'article</h2>
      <article class="border p-3">
        <h3><?php echo strip_tags( $article['titre'] ) ?></h3>
        <div class="content">
          <?php markToHtml( $article['texte'] ) ?>
        </div>
      </article>
  <?php if( !empty( $tagsLies) ) : ?>
    <h2 class="mt-3 h4">Les tags liés à l'article</h2>
    <?php foreach( $tagsLies as $tag ) : ?>
    <div class="d-flex p-2 border-bottom">
      <a class="w-50" href="updateTag.php?id=<?php echo $tag['id'] ?>" title="Modifier le tag">
        <?php echo strip_tags( $tag['titre'] ) ?>
      </a>
      <form method="post" class="w-50">
        <input type="hidden" name="id_article" value="<?php echo $article['id'] ?>">
        <div class="form-check pr-5 align-self-center">
          <input
            class="form-check-input"
            type="checkbox"
            name="id_tag"
            value="<?php echo $tag['id']?>"
            aria-label ="Cocher cette case pour supprimer la liaison"
          >
            <input type="submit" name="delete_liaison_tag" value="Supprimer la liaison"  class="btn btn-sm btn-primary">
        </div>
      </form>
    </div>
    <?php endforeach; ?>
  <?php endif; ?>

  <?php if( !empty( $mediasLies) ) : ?>
      <h2 class="mt-3 h4">Les medias liés à l'article</h2>

    <?php foreach( $mediasLies as $media ) : ?>
      <div class="card bg-light mb-3">
        <div class="card-header d-flex">
          <a class="w-50" href="updateMedia.php?id=<?php echo $media['id'] ?>" title="Modifier le media">
            <?php echo strip_tags( $media['file'] ) ?>
          </a>
          <form method="post" class="w-50">
            <input type="hidden" name="id_article" value="<?php echo $article['id'] ?>">
            <div class="form-check pr-5 align-self-center">
              <input
                class="form-check-input"
                type="checkbox"
                name="id_media"
                value="<?php echo $media['id']?>"
                aria-label ="Cocher cette case pour supprimer la liaison"
              >
                <input type="submit" name="delete_liaison_media" value="Supprimer la liaison"  class="btn btn-sm btn-primary">
            </div>
          </form>
        </div>
        <div class="card-body d-flex">
          <div class="w-50 p-2 small">
            <?php markToHtml( $media['legend'] ) ?>
          </div>
          <div class="w-50 p-2">
            <img height="100"  src="../medias/<?php echo strip_tags( $media['file'] ) ?>" alt="">
          </div>
        </div>
      </div>
    <?php endforeach; ?>

  <?php endif; ?>

    </div>
  </div>

  <div class="row">
  <?php if( !empty( $tags) ) : ?>
    <div class="col-md-6">
      <form class="mt-5" method="post">

        <fieldset class="mt-3">
          <legend>Lier des tags à l'article</legend>

            <?php foreach($tags as $tag) : ?>
            <div class="d-flex flex-row mb-2 pb-2 border-bottom">
              <div class="form-check pr-5 align-self-center">
                <input
                  id="media-<?php echo $tag['id']?>"
                  class="form-check-input"
                  type="checkbox"
                  name="tag-<?php echo $tag['id']?>"
                  value="<?php echo $tag['id']?>"
                >
                <label for="tag-<?php echo $tag['id']?>">
                  <?php echo $tag['titre']?>
                </label>
              </div>
            </div>
            <?php endforeach; ?>
            <input type="submit" name="liertags" value="Lier les tags sélectionnés"  class="btn btn-primary">
        </fieldset>
      </form>
    </div>
  <?php endif; ?>
  <?php if( !empty( $medias) ) : ?>
    <div class="col-md-6">
      <form class="mt-5" method="post">
        <fieldset class="mt-3">
          <legend>Lier des médias à l'article</legend>
            <?php foreach($medias as $media) : ?>
            <div class="d-flex flex-row mb-2 pb-2 border-bottom">
              <div class="form-check pr-5 align-self-center">
                <input
                  id="media-<?php echo $media['id']?>"
                  class="form-check-input"
                  type="checkbox"
                  name="media-<?php echo $media['id']?>"
                  value="<?php echo $media['id']?>"
                >
                <label for="media-<?php echo $media['id']?>">
                  <?php echo $media['file']?>
                </label>
              </div>
              <div class="w-25 p-2 border">
                <img height="100" src="../medias/<?php echo $media['file']?>" alt="media-<?php echo $media['id']?>">
              </div>
            </div>
            <?php endforeach; ?>
            <input type="submit" name="liermedias" value="Lier les medias sélectionnés"  class="btn btn-primary">
        </fieldset>
      </form>
    </div>
  <?php endif; ?>
  </div>
</main>

<?php include dirname(__FILE__) . '/include/footer.php'; ?>
