<?php include dirname(__FILE__) . '/include/header.php'; ?>
<main class="col-md-10">
  <nav class="mt-2" aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo BLOG_URL ?>/admin/">Accueil</a></li>
      <li class="breadcrumb-item active" aria-current="page"><?php echo strip_tags( $media['file'] ) ?></li>
    </ol>
  </nav>
  <div class="row">
    <div class="col-md-6" role="form">
      <form method="post">
        <input type="hidden" name="id" value="<?php echo $media['id'] ?>">
        <input type="hidden" name="MAX_FILE_SIZE" value="2000000">
        <fieldset>
          <legend>Modifier le média</legend>
          <?php if( !empty( $error) ) : ?>
            <p class="bg-dark text-white border border-danger p-2">
              <?php echo $error; ?>
            </p>
          <?php endif; ?>
          <div class="form-group">
            <label for="file">Changer le fichier</label>
            <input type="file" class="form-control" id="file" name="file">
          </div>
          <div class="form-group">
            <label for="name_file">Nom du fichier</label>
            <input type="text" class="form-control" id="name_file" disabled value="<?php echo strip_tags( $media['file'] ) ?>" required>
          </div>
          <div class="form-group">
            <label for="content">Légende</label>
            <textarea class="form-control" id="content" name="legend"><?php echo strip_tags( $media['legend'] ) ?></textarea>
          </div>
        </fieldset>
        <button type="submit" class="btn btn-primary">Modifier le média</button>
      </form>
    </div>
    <div class="col-md-6">
    <?php if( !empty( $articles ) ) :?>
      <h2 class="h4 mt-2">Lié aux articles suivants</h2>
      <table class="table table-striped table-bordered">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Titre</th>
          </tr>
        </thead>
        <tbody>
      <?php foreach( $articles as $article ) : ?>
          <tr>
            <td scope="row"><?php echo $article['id'] ?></td>
            <td><a href="updateArticle.php?id=<?php echo $article['id'] ?>" title="Modifier l'article"><?php echo $article['titre'] ?></a></td>
          </tr>
      <?php endforeach; ?>
        <tbody>
      </table>
    <?php endif;?>
    </div>
  </div>
  <figure class="border p-3">
    <figcaption><?php markToHtml( $media['legend'] ) ?></figcaption>
    <img class="img-fluid" src="../medias/<?php echo strip_tags( $media['file'] ) ?>" alt="">
  </figure>
</main>

<?php include dirname(__FILE__) . '/include/footer.php'; ?>
