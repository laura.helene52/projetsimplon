<?php
// Afficher toutes les erreurs : à commenter en production
ini_set('display_errors', 1);
error_reporting(E_ALL);

include_once('../config/connect.php');
require_once '../inc/markdown/Markdown.inc.php';
use Michelf\Markdown;

function markToHtml( $content )
{
  echo Markdown::defaultTransform(htmlspecialchars( $content ));
}


$contexte = 'updateArticle';
$title= 'Modifier article';

/*
    REQUETE RUBRIQUES
    Récupérer toutes les rubriques pour le select
*/

$query = $pdo->prepare('SELECT id,titre FROM rubriques');
$query->execute();
$rubriques = $query->fetchAll();
$query->closeCursor();


/*
    REQUETE ARTICLE
    Récupérer les données de l'article
*/

$query = $pdo->prepare("
  SELECT
  articles.id AS id,
  articles.titre AS titre,
  articles.texte AS texte,
  rubriques.titre AS rub_titre,
  articles.id_rub AS id_rub,
  DATE_FORMAT(date, '%d/%m/%Y') AS date
  FROM articles
  INNER JOIN rubriques ON articles.id_rub = rubriques.id
    WHERE articles.id = ?
");
$query->execute(array( $_GET['id'] ) );
$article = $query->fetch(); // Récupérer 1 ligne
$query->closeCursor();


/*
    REQUETE MEDIAS LIES
    Récupérer tous les medias liés
*/

$query = $pdo->prepare("
  SELECT * FROM medias
  INNER JOIN medias_liaisons ON medias_liaisons.id_media = medias.id
  WHERE medias_liaisons.id_article = ?
");
$query->execute(array( $_GET['id'] ) );
$mediasLies = $query->fetchAll();
$query->closeCursor();


/*
    REQUETE TAGS LIES
    Récupérer tous les tags liés
*/

$query = $pdo->prepare("
  SELECT * FROM tags
  INNER JOIN tags_liaisons ON tags_liaisons.id_tag = tags.id
  WHERE tags_liaisons.id_article = ?
");
$query->execute(array( $_GET['id'] ) );
$tagsLies = $query->fetchAll();
$query->closeCursor();


/*
    REQUETE MEDIAS pour le formulaire ajout d'un média à l'article
    Récupérer tous les medias
*/

$query = $pdo->prepare("
  SELECT * FROM medias
");
$query->execute(array( $_GET['id'] ) );
$medias = $query->fetchAll();
$query->closeCursor();

/*
  Supprimer de $medias ceux déjà liés à l'article
    -> Comparer $medias et $mediasLies
  On boucle sur chaque ligne de $medias
    on boucle sur chaque ligne de $mediasLies
    si les id sont identiques (media déjà lié)
      on suppprime ce media lié de $medias
        donc, on supprime la ligne $medias[$i]

  En fait, $media['id'] = $medias[$i]['id']
*/
foreach ($medias as $i => $media)
{
  foreach ($mediasLies as $mediaLie) {
    if( $media['id'] == $mediaLie['id'] )
    {
      unset( $medias[$i] ); // Détruit la ligne
    }
  }
}


/*
    REQUETE TAGS pour le formulaire ajout d'un tag à l'article
    Récupérer tous les medias
*/

$query = $pdo->prepare("
  SELECT * FROM tags
");
$query->execute(array( $_GET['id'] ) );
$tags = $query->fetchAll();
$query->closeCursor();

foreach ($tags as $i => $tag)
{
  foreach ($tagsLies as $tagLie) {
    if( $tag['id'] == $tagLie['id'] )
    {
      unset( $tags[$i] ); // Détruit la ligne
    }
  }
}


/*
    FORMULAIRE DE TRAITEMENT DE MISE A JOUR DE L'ARTICLE
    Requête de mise à jour de l'article
*/

if( array_key_exists('title', $_POST) )
{
  $title = $_POST['title'];
  $content = $_POST['content'];
  $id_rub = $_POST['id_rub'];
  $id = $_POST['id'];

  $sql = '
    UPDATE articles SET
    titre = :titre, texte = :texte, id_rub = :id_rub
    WHERE id = :id
  ';

  $query = $pdo->prepare($sql);
  $query->execute(
      array(
        ':titre' => $title,
        ':texte' => $content,
        ':id_rub' => $id_rub,
        ':id' => $id
      )
  );

  $query->closeCursor();

  // On redirige l'internaute vers la même page ( contrôleur : updateArticle.php)
  header('Location:'.BLOG_URL . '/admin/updateArticle.php?id=' . $id);
  exit;
}

/*
    FORMULAIRE DE TRAITEMENT DES LIAISONS MEDIAS
    Requête d'insertion dans medias_liaisons
*/

if( array_key_exists('liermedias', $_POST) )
{
  $id_article = intval($_GET['id']);
  unset($_POST['liermedias']);

  $query = $pdo->prepare("
    INSERT INTO medias_liaisons (id_media,id_article) VALUES (?, ?)
  ");

  foreach ( $_POST as $idmedia )
  {
    $query->execute( array($idmedia,$id_article) );
  }

  $query->closeCursor();

  header('Location:'.BLOG_URL . '/admin/updateArticle.php?id=' . $id_article);

}

/*
    FORMULAIRE DE TRAITEMENT DES LIAISONS TAGS
    Requête d'insertion dans tags_liaisons
*/

if( array_key_exists('liertags', $_POST) )
{
  $id_article = intval($_GET['id']);
  unset($_POST['liertags']);

  $query = $pdo->prepare("
    INSERT INTO tags_liaisons (id_tag,id_article) VALUES (?, ?)
  ");

  foreach ( $_POST as $idtag )
  {
    $query->execute( array($idtag,$id_article) );
  }

  $query->closeCursor();

  header('Location:'.BLOG_URL . '/admin/updateArticle.php?id=' . $id_article);

}


/*
    FORMULAIRE DE SUPPRESSION D'UNE LIAISON MEDIA
    Requête de mise à jour de medias_liaisons
*/

if( array_key_exists('delete_liaison_media', $_POST) )
{
  $id_article = intval($_POST['id_article']);
  $id_media = intval($_POST['id_media']);

  $query = $pdo->prepare("
    DELETE FROM medias_liaisons
    WHERE id_media = ? AND id_article = ?
  ");
  $query->execute( array($id_media,$id_article) );
  $query->closeCursor();

  header('Location:'.BLOG_URL . '/admin/updateArticle.php?id=' . $id_article);

}

/*
    FORMULAIRE DE SUPPRESSION D'UNE LIAISON TAG
    Requête de mise à jour de tags_liaisons
*/

if( array_key_exists('delete_liaison_tag', $_POST) )
{
  $id_article = intval($_POST['id_article']);
  $id_tag = intval($_POST['id_tag']);

  $query = $pdo->prepare("
    DELETE FROM tags_liaisons
    WHERE id_tag = ? AND id_article = ?
  ");
  $query->execute( array($id_tag,$id_article) );
  $query->closeCursor();

  header('Location:'.BLOG_URL . '/admin/updateArticle.php?id=' . $id_article);

}



include './templates/' . $contexte. 'View.php';
