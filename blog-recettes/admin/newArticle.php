<?php
// Afficher toutes les erreurs : à commenter en production
ini_set('display_errors', 1);
error_reporting(E_ALL);

include_once('../config/connect.php');


$contexte = 'newArticle';
$title= 'Nouvel article';

/*
    REQUETE RUBRIQUES
    Récupérer toutes les rubriques pour le select

*/

$query = $pdo->prepare('SELECT id,titre FROM rubriques');
$query->execute();
$rubriques = $query->fetchAll();
$query->closeCursor();


if( array_key_exists('title', $_POST) )
{
  $title = $_POST['title'];
  $content = $_POST['content'];
  $id_rub = intval( $_POST['id_rub'] );

  $query = $pdo->prepare("INSERT INTO articles (titre, texte, id_rub) VALUES (?, ?, ?)");
  $query->execute( array($title, $content, $id_rub) );

  //On récupère l'id nouvellement créée afin de rediriger sur la page de visualisation de ce nouvel article
  $id = $pdo->lastInsertId();

  // On redirige l'internaute vers la page de modification et de visualisation de l'article (contrôleur : updateArticle.php)
  header('Location:'.BLOG_URL . '/admin/updateArticle.php?id=' . $id);
  exit;
}

include './templates/' . $contexte. 'View.php';
