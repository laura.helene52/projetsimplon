<?php
// Afficher toutes les erreurs : à commenter en production
ini_set('display_errors', 1);
error_reporting(E_ALL);

include_once('../config/connect.php');
require_once '../inc/markdown/Markdown.inc.php';
use Michelf\Markdown;

function markToHtml( $content )
{
  echo Markdown::defaultTransform(htmlspecialchars( $content ));
}


$contexte = 'updateRubrique';
$title= 'Modifier rubrique';

/*
    REQUETE RUBRIQUE
    Récupérer les données de la rubrique
*/

$query = $pdo->prepare('
  SELECT * FROM rubriques
  WHERE id = ?
');
$query->execute(array( $_GET['id'] ) );
$rubrique = $query->fetch(); // Récupérer 1 ligne
$query->closeCursor();

/*
    REQUETE ARTICLES dr la RUBRIQUE
    Récupérer les articles de la rubrique
*/

$query = $pdo->prepare('
  SELECT * FROM articles
  WHERE id_rub = ?
');
$query->execute(array( $_GET['id'] ) );
$articles = $query->fetchAll(); // Récupérer toutes les lignes
$query->closeCursor();


if( array_key_exists('title', $_POST) )
{
  $title = $_POST['title'];
  $content = $_POST['content'];
  $id = $_POST['id'];

  $sql = '
    UPDATE rubriques SET
    titre = :titre, texte = :texte
    WHERE id = :id
  ';

  $query = $pdo->prepare($sql);
  $query->execute(
      array(
        ':titre' => $title,
        ':texte' => $content,
        ':id' => $id
      )
  );

  // On redirige l'internaute vers la même page ( contrôleur : updateArticle.php)
  header('Location:'.BLOG_URL . '/admin/updateRubrique.php?id=' . $id);
  exit;
}

include './templates/' . $contexte. 'View.php';
