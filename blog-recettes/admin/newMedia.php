<?php
// Afficher toutes les erreurs : à commenter en production
ini_set('display_errors', 1);
error_reporting(E_ALL);

include_once('../config/connect.php');


$contexte = 'newMedia';
$title= 'Nouveau média';

$error ='';

if( !empty($_POST) && is_uploaded_file( $_FILES['file']['tmp_name'] ) )
{

  $taille_maxi = 2000000;

  //On fait un tableau contenant les extensions autorisées.
  //Comme il s'agit d'une image, on ne prend que des extensions d'images.
  $extensions = array('.png', '.gif', '.jpg', '.jpeg','.JPG','.JPEG','.PNG','.GIF');
  // récupère la partie de la chaine à partir du dernier . pour connaître l'extension.
  $extension = strrchr($_FILES['file']['name'], '.');

  switch( $_FILES['file']['error'] )
  {
    case 0 :
    $error = "Le fichier a été téléchargé avec succès"; // UPLOAD_ERR_INI_SIZE: 1
    break;

    case 1 :
    $error = "Fichier dépassant la taille maximale autorisée par le serveur"; // UPLOAD_ERR_INI_SIZE: 1
    break;

    case 2 :
    $error = "Fichier dépassant la taille maximale autorisée"; // UPLOAD_ERR_FORM_SIZE: 2
    break;

    case 3 :
    $error = "Fichier transféré partiellement"; // UPLOAD_ERR_PARTIAL: 3
    break;

    case 4 :
    $error = "Aucun fichier n'a été sélectionné depuis votre système !";
    break;

    case 6 :
    $error = "Pas de répertoire temporaire"; // UPLOAD_ERR_NO_TMP_DIR: 6
    break;

    case 7 :
    $error = "Ecriture du fichier impossible"; // UPLOAD_ERR_CANT_WRITE: 7
    break;

    case 8 :
    $error = "Erreur d'extension"; // UPLOAD_ERR_EXTENSION: 8
    break;

    default :
    $error = "Erreur inconnue : ".$_FILES['file']['error'];
  }

  if ($_FILES['file']['error']==0)
  {
    if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
    {
      $error = 'Vous devez uploader un fichier de type png, gif, jpg ou jpeg';
    }
    elseif ($_FILES['file']['size'] > $taille_maxi)
    {
      $error = "Fichier dépassant la taille maximale autorisée";
    }
    else // On traite
    {
      $dossier = '../medias/';
      $file = str_replace( ' ','_',$_FILES['file']['name']);
      $legend = $_POST['legend'];

      if( move_uploaded_file( $_FILES['file']['tmp_name'], $dossier . $file ) )
      {

        $error = "Enregistrement sur medias : OK";


        $query = $pdo->prepare("INSERT INTO medias (file, legend) VALUES (?, ?)");
        $query->execute( array($file, $legend) );

        //On récupère l'id nouvellement créée afin de rediriger sur la page de visualisation de ce nouvel article
        $id = $pdo->lastInsertId();

        // On redirige l'internaute vers la page de modification et de visualisation du média (contrôleur : updateMedia.php)
        header('Location:'.BLOG_URL . '/admin/updateMedia.php?id=' . $id);
        exit;
      }
      else
      {
        $error = 'Echec de l\'enregistrement sur ' . $dossier;
      }
    }
  }
}

include './templates/' . $contexte. 'View.php';
