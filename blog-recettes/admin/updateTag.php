<?php
// Afficher toutes les erreurs : à commenter en production
ini_set('display_errors', 1);
error_reporting(E_ALL);

include_once('../config/connect.php');
require_once '../inc/markdown/Markdown.inc.php';
use Michelf\Markdown;

function markToHtml( $content )
{
  echo Markdown::defaultTransform(htmlspecialchars( $content ));
}


$contexte = 'updateTag';
$title= 'Modifier tag';

/*
    REQUETE TAG
    Récupérer les données du tag
*/

$query = $pdo->prepare('
  SELECT * FROM tags
  WHERE id = ?
');
$query->execute(array( $_GET['id'] ) );
$tag = $query->fetch(); // Récupérer 1 ligne
$query->closeCursor();

/*
    REQUETE ARTICLES lié au TAG
    Récupérer les articles
*/

$query = $pdo->prepare('
  SELECT * FROM tags
  INNER JOIN tags_liaisons ON tags_liaisons.id_tag = tags.id
  WHERE tags_liaisons.id_article = ?
');
$query->execute(array( $_GET['id'] ) );
$tags = $query->fetchAll(); // Récupérer toutes les lignes
$query->closeCursor();


if( array_key_exists('title', $_POST) )
{
  $title = $_POST['title'];
  $id = $_POST['id'];

  $sql = '
    UPDATE tags SET
    titre = :titre
    WHERE id = :id
  ';

  $query = $pdo->prepare($sql);
  $query->execute(
      array(
        ':titre' => $title,
        ':id' => $id
      )
  );

  // On redirige l'internaute vers la même page ( contrôleur : updateArticle.php)
  header('Location:'.BLOG_URL . '/admin/updateTag.php?id=' . $id);
  exit;
}

include './templates/' . $contexte. 'View.php';
