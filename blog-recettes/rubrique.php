<?php
include_once('config/connect.php');
include_once('requetes/fonctions.php');

$template = dirname(__FILE__) . '/' . THEME_NAME . '/rubrique.php';

if( !empty( $_GET['id']) )
{
  include('requetes/rubrique.php');

  if( empty( $rubrique ) )
  {
    header('Location:'.BLOG_URL  . '/404.php');
    return;
  }

  if ( !file_exists($template) )
  {
    $template = dirname(__FILE__) . '/' . THEME_NAME . '/home.php';
  }
  include($template);
}
else
{
  header('Location:'.BLOG_URL);
}
