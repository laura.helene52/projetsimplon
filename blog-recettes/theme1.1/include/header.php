<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <?php if(is_home()) : ?>
        <title><?php bloginfo('title');?></title>
        <meta name="description" content="<?php bloginfo('description');?>">

        <?php elseif (is_article()): ?>
        <title><?php the_title();?></title>
        <meta name="description" content="<?php the_content('',150);?>">

        <?php elseif (is_rubrique()): ?>
        <title><?php afficheTitreRubrique( $_GET['id'])?></title>
        <meta name="description" content="<?php afficheTitreRubrique($_GET['id'])?>">


        <?php endif; ?>



        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php afficheURLTheme();?>/css/styles.css">

    </head>
    <body>
      <header>
        <h1> <?php bloginfo('title'); ?></h1>
        <nav>
          <a class="menu-link" href="<?php bloginfo('url'); ?>/admin/">administration</a>
          <a href="./" class="menu-item">home</a>
          <?php
        nav_menu(

          array(
            'liste' =>'ul'
          )
        );
        ?>
        </nav>

      </header>
