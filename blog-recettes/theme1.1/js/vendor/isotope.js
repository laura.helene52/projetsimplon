'use strict';

$(function()
{

  function onClickExecute(event)
  {
    event.preventDefault();

    var tag;

    tag = $(this).data('isotope');
    console.log(tag);

    $('#isotope-nav .active').removeClass('active');
    $(this).addClass('active');

    if( typeof(tag) == 'undefined')
    {
      $('#isotope-container article').show();
      return;
    }

    $('#isotope-container article').each( function(){

      if($(this).hasClass(tag))
      {
        $(this).show('slow');
      }
      else
      {
        $(this).hide(500);
      }


    });

  }

  $('#isotope-nav').on('click','a', onClickExecute );


});
