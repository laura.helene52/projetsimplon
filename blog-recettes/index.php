<?php
include_once('config/connect.php');
include_once('requetes/fonctions.php');

$template = dirname(__FILE__) . '/' . THEME_NAME . '/home.php';

if (file_exists($template))
{
  include('requetes/accueil.php');
  include($template);
}
else
{
    echo '<p>Le fichier : <code>' . THEME_NAME . '/home.php</code> n\'existe pas, il faut le créer sur le modèle d\'un document html, avec l\'extension .php.</p>';
}
