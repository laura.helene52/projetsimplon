'use strict';

(function() {

  // Sortir si vieu navigateur
  if ( !('querySelector' in document && 'addEventListener' in window) ) {
    return;
  }

  // Cibler la balise html pour supprimer la class no-js et ajouter js-enabled
  window.document.documentElement.classList.remove('no-js');
  window.document.documentElement.classList.add('js-enabled');

  function toggleNav() {

    // Define targets
    var button = document.querySelector('.button-burger');

    // Si on trouve le bouton
    if ( button ) {
      button.addEventListener('click',
      function (e) {
        var targetId = this.dataset.toggle; //Contenu de l'attribut data-toggle du bouton
        document.getElementById(targetId).classList.toggle('is-open');
        e.preventDefault();
      }, false );
    }
  }
  toggleNav();

}());

(function() {

  var banner;
  var position;
  var h;

  banner = document.querySelector('.banner');
  //h = document.querySelector('.logo').clientHeight;
  h = document.querySelector('.logo').offsetTop;

  window.addEventListener('scroll', function(){

    position = window.scrollY;

    if( position > h )
    {
      banner.classList.remove('home');
    }
    else
    {
      banner.classList.add('home');
    }

  });
  
}());
