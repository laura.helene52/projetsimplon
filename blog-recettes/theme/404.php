<?php include('include/header.php') ?>
<main>
	<header>
		<h1>Page non trouvée</h1>
		<p>Erreur 404</p>
	</header>

	<section>
	  <div class="container">
			<p>Nous nous sommes perdus dans nos id : la page demandée a disparu de la base d'informations.</p>
		</div>
	</section>
</main>
<?php include('include/footer.php') ?>
