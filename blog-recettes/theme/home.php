<?php include('include/header.php') ?>
<main itemprop="mainContentOfPage" class="site-main">

	<header class="page-header">
		<div class="container">
			<h1><?php bloginfo('title') ?></h1>
			<p><?php bloginfo('description') ?></p>
		</div>
	</header>

	<section class="hfeed">
	  <div class="container">

	<?php if( !empty($articles) ) : ?>

		<p class="h3 m-0">Les derniers articles</p>

		<?php foreach( $articles as $article ) : the_post() ?>
		  <article id="post-<?php the_id(); ?>" class="hentry clearfix" itemscope itemtype="http://schema.org/Article">
				<header class="article-header">
					<h2 itemprop="name" class="hentry-title"><?php the_title() ?></h2>
					<p class="meta">
						Posté le
						<time class="published" itemprop="dateCreated" datetime="<?php the_date_iso() ?>"><?php the_date() ?></time>
						dans la rubrique <span itemprop="about"><?php afficheTitreRubrique( get_the_id_rub( get_the_id() ) ) ?></span>
					</p>
				</header>
				<div class="entry-summary" itemprop="articleBody">
					<?php echo cutContent( get_the_content( get_the_id() ) , 50 ) ?>
				</div>
				<a href="<?php the_url() ?>" itemprop="url" rel="bookmark">Lire la suite</a>
				<div class="article-media">
					<?php afficheMedias(); ?>
				</div>
		  </article>

		<?php endforeach; ?>

	<?php else : ?>
			<p>Il n'y a pas d'articles à afficher</p>
	<?php endif; ?>

		</div>
	</section>
</main>
<?php include('include/footer.php') ?>
