<?php include('include/header.php') ?>
<main itemprop="mainContentOfPage" class="site-main">

<?php if( !empty($articles) ) : ?>
	<?php foreach( $articles as $article ) : the_post() ?>
	<article id="post-<?php the_id(); ?>" class="hentry clearfix" itemscope itemtype="http://schema.org/Article">

		<header class="page-header">
			<div class="container">
				<h1  itemprop="name" class="hentry-title"><?php the_title() ?></h1>
				<p class="meta">
					Posté le
					<time class="published" itemprop="dateCreated" datetime="<?php the_date_iso() ?>"><?php the_date() ?></time>
					dans la rubrique <span itemprop="about"><?php afficheTitreRubrique( get_the_id_rub( get_the_id() ) ) ?></span>
				</p>
			</div>
		</header>
		
		<div class="container">
			<div class="entry-content">
				<?php the_content() ?>
			</div>
			<div class="article-media">
				<?php afficheMedias(); ?>
			</div>
		</div>
		
	</article>
	<?php endforeach; ?>	
<?php endif; ?>

</main>
<?php include('include/footer.php') ?>
