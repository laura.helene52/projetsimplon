<?php
include_once('config/connect.php');
include_once('requetes/fonctions.php');

$template = dirname(__FILE__) . '/' . THEME_NAME . '/404.php';

if ( !file_exists($template) )
{
  $template = dirname(__FILE__) . '/' . THEME_NAME . '/home.php';
}
include($template);
