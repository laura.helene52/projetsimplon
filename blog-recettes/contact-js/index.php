<?php include('inc/email.php'); ?>
<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Contact</title>
        <meta name="description" content="Un formulaire de contact envoyé par mail">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Oswald:400,500" rel="stylesheet">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/fontello.css">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
      <main>
        <h1><a href="./">Formulaire de contact</a></h1>
        <div id="contact" class="section" role="form">
          <?php include('inc/form-contact.php'); ?>
        </div>
      </main>
      <script src="js/main.js"></script>
    </body>
</html>
