<?php

function sendMail($recipient,$name,$email,$object,$message)
{

	$name   = utf8_decode($name);
	$object = utf8_decode($object);
	$message = utf8_decode( nl2br($message) );

	if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $recipient)) // On filtre les serveurs qui rencontrent des bogues.
	{
		$passage_ligne = "\r\n";
	}
	else
	{
		$passage_ligne = "\n";
	}
	//=====Déclaration des messages au format texte et au format HTML.
	$message_txt = $message;
	$message_html = '<html><head></head><body>'.$message.'</body></html>';
	//==========

	//=====Création de la boundary
	$boundary = "-----=".md5(rand());
	//==========

	//=====Définition du sujet.
	$object = $object;
	//=========

	//=====Création du header de l'e-mail.
	$header = 'From: '.$name.'<'.$email.'>'.$passage_ligne;
	$header.= 'Reply-to: '.$name.'<'.$email.'>'.$passage_ligne;
	$header.= "MIME-Version: 1.0".$passage_ligne;
	$header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
	//==========

	//=====Création du message.
	$message = $passage_ligne."--".$boundary.$passage_ligne;
	//=====Ajout du message au format texte.
	$message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
	$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
	$message.= $passage_ligne.$message_txt.$passage_ligne;
	//==========
	$message.= $passage_ligne."--".$boundary.$passage_ligne;
	//=====Ajout du message au format HTML
	$message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
	$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
	$message.= $passage_ligne.$message_html.$passage_ligne;
	//==========
	$message.= $passage_ligne."--".$boundary."--".$passage_ligne;
	$message.= $passage_ligne."--".$boundary."--".$passage_ligne;
	//==========

	/* On vide les valeurs : */
	$_POST['name']='';
	$_POST['object']='';
	$_POST['message']='';
	$_POST['email']='';

	//=====Envoi de l'e-mail via la fonction mail() de php.
	return mail($recipient,$object,$message,$header);
}
