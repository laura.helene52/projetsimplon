<?php

/*
 Utilisation des sessions pour gérer les messages en fonction du traitement
 notamment si la page est redirigée par header('location:')
 car, dans ce cas, c'est comme si le formulaire n'avait pas été posté

 Nota : session et header sont des fonctions d'en-tête http :
  elles doivent être exécutées avant l'envoi du fichier -> avant <!doctype>


  2 cas possibles avec redirection : affichage d'un message à la place du formulaire
    1 - le formulaire a bien été posté
    2 - une erreur technique est survenue lors de l'envoi

  1 cas possible sans redirection -> erreur de saisie
    On représente le formulaire :
      - en affichant les valeurs saisies lors du post précédent (attribut value)
      - en mettant en relief les champs présentant l'erreur avec une classe
      - en affichant l'erreur sous les champs en erreur

*/
session_start();

// Adresse email du destinataire
$recipient  ='veronique.cuomo@gmail.com';

/*
 Variables dans lesquelles les données postées seront récupérées
 Elles sont affichées dans l'attribut value de chaque champ
*/
$name       = '';
$object     = '';
$email      = '';
$message    = '';

/*
  Erreurs pour chaque champ
  Si un champ présente une erreur, on rajoute une ligne dans le tableau correspondant au name du champ
    -> ajout de la class="error" pour styler
    -> affichage de l'erreur sous le champ
*/
$error      = array();

/*
  Attribut action du formulaire
  Url de redirection
  On y rajoutera un code erreur en paramètre à la suite

  Notez la présence de l'ancre :
    sur un onepage, si le formulaire n'est pas au début
    il faut que le formulaire et/ou le message soient visibles au chargement
*/
$url = $_SERVER['PHP_SELF'] .'#contact';

/*
  Tableau pour stocker les notifications
  Chaque indice sera stocké dans une varaible de session une fois le formulaire posté
*/
$notification = array(
  'help'  => '<p>Merci de bien vouloir compléter le formulaire suivant<br>(<small>Toutes les informations sont requises</small>)</p>',
  'ok'   => '<p>Votre message a été envoyé avec succès, nous y répondrons dès que possible.</p>',
  'er'   => '<p>Un problème technique est survenu lors de l\'envoi du message</p>',
  'nok'  => '<p>Le formulaire présente des erreurs de saisie</p>'
);

/*
  Si le formulaire est posté :

  On vérifie la validité des saisies
    si pas d'erreur on passe à la fonction sendMail

*/

if ( !empty($_POST) )
{

  /*
    Pour chaque champ :
      on vérifie si il n'est pas vide
        - on assigne les variables déclarées au début ($name,$email, etc)
          en fait, on utilise la clé de $_POST comme variable dynamique
            Pour la ligne $_POST['name'], $key = name -> $$key devient $name

      sinon, on stocke une erreur
        Par exemple : $error['name']
  */

  foreach ($_POST as $key => $value)
  {
    $value = strip_tags( trim( $value ) );
    if( !empty( $value ) )
    {
      $$key = $value;
    }
    else
    {
      $error[$key] = 'Le champ est requis';
    }
  }

  /*
    On peut faire d'autres vérifications spécifiques
    Par exemple : vérifier si l'email est valide
  */
  if ( !filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
    $error['email'] = 'L\'adresse email n\'est pas valide';
  }

  /*
   Si il n'y a pas d'erreur, $error est resté vide
    -> on envoie et on redirige pour éviter de resposter le formulaire
  */
  if( empty( $error ) )
  {
    // script qui construit le message et l'envoie
    require('sendMail.php');

    /*
      Pour éviter un else après le tests d'envoi
      On stocke l'index de l'erreur d'envoi dans la session
      (dans le tableau notification, elle correspond à $notification['er'])
    */
    $_SESSION['contact'] = 'er';

    // Si il n'y a pas de problème lors de l'envoi : sendMail renvoie true
    if( sendMail($recipient,$name,$email,$object,$message) )
    {
      /* Tout c'est bien passé, donc : */
      $_SESSION['contact'] = 'ok';
    }
    // Problème ou pas lors de l'envoi, on redirige
    header('location:'.$url);
    exit;
  }
}
