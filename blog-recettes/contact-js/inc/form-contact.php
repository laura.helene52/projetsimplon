<?php if( empty( $_SESSION['contact'] ) ) : ?>
<form
  method="post"
  action="<?php echo $url ?>"
  novalidate>
  <fieldset class="effect">
    <legend>Nous écrire</legend>
    <?php if( empty($error) ) : ?>
    <div class="message"><?php echo $notification['help'] ?></div>
    <?php else : ?>
    <div class="message"><?php echo $notification['nok'] ?></div>
    <?php endif; ?>
    <p class="field<?php if( !empty( $error['name'] ) ) : echo ' error'; endif; ?>">
      <label for="name">Votre nom</label>
      <input class="form-field" type="text" id="name" name="name" value="<?php echo $name ?>" required>
      <?php if( !empty( $error['name'] ) ) : printf('<small>%1$s</small>' , $error['name'] ); endif; ?>
    </p>
    <p class="field<?php if( !empty( $error['email'] ) ) : echo ' error'; endif; ?>">
      <label for="email">Votre adresse email</label>
      <input class="form-field" type="email" id="email" name="email" value="<?php echo $email ?>" required>
      <?php if( !empty( $error['email'] ) ) : printf('<small>%1$s</small>' , $error['email'] ); endif; ?>
    </p>
    <p class="field<?php if( !empty( $error['object'] ) ) : echo ' error'; endif; ?>">
      <label for="object">Objet de votre message</label>
      <input class="form-field" type="text" id="object" name="object" value="<?php echo $object ?>" required>
      <?php if( !empty( $error['object'] ) ) : printf('<small>%1$s</small>' ,$error['object'] ); endif; ?>
    </p>
    <p class="field<?php if( !empty( $error['message'] ) ) : echo ' error'; endif; ?>">
      <label for="message">Votre message</label>
      <textarea class="form-field" id="message" name="message" required><?php echo $message; ?></textarea>
      <?php if( !empty( $error['message'] ) ) : printf('<small>%1$s</small>' ,$error['message'] ); endif; ?>
    </p>
    <p class="button">
      <button type="submit">Envoyer</button>
    </p>
  </fieldset>
</form>
<?php else : $index = $_SESSION['contact']; ?>
<div class="notification effect"><?php echo $notification[$index] ?></div>
<?php endif; ?>
