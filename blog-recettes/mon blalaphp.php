<section>
<?php if( !empty ($articles) ) : ?>

	<h2>Les derniers articles</h2>
	<?php foreach($articles as $article) : the_post()?>
	<article>
		<h3><?php the_title() ?></h3>
		<p>Publié le<?php the_date() ?></p>
		<p>Dans la rubrique:<?php afficheTitreRubrique( get_the_id_rub(  get_the_id() ) ) ; ?></p>
		<a href="article.php?id=<?php the_id(); ?>">Lire la suite de <?php the_title(); ?></a>
		<?php afficheMedias()?>
	</article>

	<?php endforeach; ?>

<?php else :  ?>
<p>rien</p>
<?php endif; ?>

</section>
