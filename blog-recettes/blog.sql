-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 08 mars 2018 à 12:40
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `id_rub` smallint(6) NOT NULL,
  `titre` varchar(256) NOT NULL,
  `texte` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_rub` (`id_rub`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`id`, `id_rub`, `titre`, `texte`, `date`) VALUES
(3, 1, 'Soupe de pêches au romarin', 'Cette soupe de pêches au romarin est très rafraîchissante. Rapide et facile à réalisée, il suffit de la placer au réfrigérateur avant de la servir et le tour est joué. Vous pouvez donc réaliser cette recette bien à l\'avance, pratique lorsque vous recevez !\r\nInfos pratiques\r\nNombre de personnes6\r\nTemps de préparation20 minutes\r\nTemps de cuisson10 minutes\r\nDegré de difficultéFacile\r\nCoûtBon marché\r\nIngrédients\r\n6 pêches\r\n250 g de groseilles\r\n3 brins de romarin\r\n100 g de sucre roux en poudre\r\nÉtapes\r\n1. Faites bouillir 80 cl d’eau avec les groseilles (non égrappées), le sucre et le romarin pendant 10 min à feu vif, puis filtrez le sirop au travers d’une passoire, en écrasant bien les groseilles.\r\n\r\n2. Faites bouillir de l’eau dans une grande casserole, plongez-y les pêches 30 secondes puis pelez-les sous l’eau froide.\r\n\r\n3. Mettez les pêches entières dans un saladier et versez dessus le sirop chaud.\r\n\r\n4. Laissez refroidir et réservez au réfrigérateur jusqu’au moment de servir, bien frais.\r\n\r\nRecette parue dans le numéro 152\r\nLe bon accord vin\r\nCouleur du vin : blanc\r\nAppellation : un cassis\r\nRégion : Provence et Corse\r\n\r\nConseils\r\nVous pouvez remplacer les pêches par d\'autres fruits, comme dans notre recette de soupe de fraises par exemple !\r\n\r\n', '2018-03-05 10:09:51'),
(4, 1, 'Compotée de pêches au beurre salé et aux amandes', 'Cette compotée de pêches au beurre salé et aux amandes est une recette idéale si vous avez des pêches un peu trop mûres ou si vous aimez les pêches tout simplement. La compotée est servie avec de la nougatine maison.\r\nInfos pratiques\r\nNombre de personnes4\r\nTemps de préparationPréparation et cuisson : 25 minutes\r\nTemps de cuissonPréparation et cuisson : 25 minutes\r\nDegré de difficultéFacile\r\nCoûtBon marché\r\nIngrédients\r\n1 kg de pêches (jaunes et blanches)\r\n30 g de beurre salé\r\n50 g d’amandes blanches\r\n50 g de sucre roux en poudre\r\n75 g de sucre blanc en poudre\r\n1/2 cuil. à café de gingembre en poudre\r\n1 cuil. à café d’huile\r\nÉtapes\r\n1. Pelez les pêches (ébouillantez-les quelques secondes si nécessaire pour faciliter l’opération). Coupez-les en quatre en les dénoyautant, recoupez les quartiers en gros cubes.\r\n\r\n2. Dans une cocotte, faites fondre le sucre roux avec le beurre pour obtenir un caramel. Ajoutez les pêches et le gingembre, remuez 5 à 7 min sur feu moyen, jusqu’à ce que tous les morceaux soient enrobés, mais encore un peu fermes. Versez dans un compotier, laissez refroidir.\r\n\r\n3. Pendant ce temps, faites fondre le sucre blanc avec 1 cuillerée à soupe d’eau dans une casserole à fond épais. Secouez régulièrement la casserole pour répartir la chaleur. Dès qu’un caramel blond se forme, sortez la casserole du feu, ajoutez les amandes et remuez avec une spatule.\r\n\r\n4. Versez le tout sur une feuille de papier sulfurisé huilée, laissez refroidir cette “nougatine”. Cassez la nougatine refroidie avec les doigts, parsemez-en la compotée de pêches et servez à température ambiante.\r\n\r\nRecette parue dans le numéro 152\r\nLe bon accord vin\r\nType de vin : Vin moelleux\r\nCouleur du vin : blanc\r\nAppellation : un montlouis-sur-loire\r\nRégion : Vallée de la Loire\r\n\r\nConseils\r\nC’est bon avec… … une boule de glace au lait d’amandes ou au caramel au beurre salé.', '2018-03-05 10:12:26'),
(5, 3, 'Sabayon de pêches aux amaretti', 'sabayon de pêches aux amaretti,sabayon de nectarine, sabayon,sabayon aux amaretti, dessert\r\nInfos pratiques\r\nNombre de personnes6\r\nTemps de préparation20 minutes\r\nTemps de cuisson30 minutes\r\nDegré de difficultéConfirmé\r\nCoûtAbordable\r\nIngrédients\r\n3 pêches\r\n12 amaretti (petits macarons italiens, chez les traiteurs italiens)\r\n6 jaunes d’œufs\r\n60 cl de vin blanc sec\r\n90 g de sucre\r\n1/2 citron\r\n1 cuil. à soupe de noix de coco râpée\r\nÉtapes\r\n1. Rincez les pêches, ouvrez-les en 2 et ôtez les noyaux.\r\n\r\n2. Pressez le demi-citron.\r\n\r\n3. Dans une casserole, portez à ébullition le vin, le jus de citron et le sucre. \r\nFaites-y pocher 5 min les demi-pêches puis sortez-les à l’aide d’une écumoire. \r\nRépartissez-les dans des plats à four individuels, côté bombé vers le fond.\r\n\r\n4. Faites réduire le sirop de moitié à feu vif, laissez refroidir. \r\nFaites chauffer une casserole d’eau pour préparer un bain-marie.\r\n\r\n5. Dans une jatte, fouettez les jaunes d’œufs puis incorporez- leur le sirop refroidi. \r\nMettez la jatte sur le bain-marie et fouettez sans arrêt jusqu’à ce que la crème soit épaisse et onctueuse (environ 10 min), sans jamais laisser bouillir.\r\n\r\n6. Retirez du feu. \r\nConcassez les amaretti et garnissez- en le cœur des demi-pêches puis nappez de sabayon. \r\nFaites dorer quelques minutes (en surveillant) sous le gril du four, parsemez de noix de coco râpée et servez aussitôt.\r\n\r\nRecette parue dans le numéro 634\r\nLe bon accord vin\r\nType de vin : Vin tranquille\r\nCouleur du vin : blanc\r\nAppellation : un montlouis\r\nRégion : Vallée de la Loire', '2018-03-05 10:15:43'),
(6, 1, 'Soupe de pêches au vin doux et aux épices', 'cannelle, clou de girofle, gingembre, vin blanc, soupe, froid, sucré, verrine, pêche, dessert, cuisine rapide, dessert, sucre, clou de girofle, cannelle, gingembre\r\nInfos pratiques\r\nNombre de personnes6\r\nTemps de préparation10 minutes\r\nTemps de cuisson15 minutes\r\nDegré de difficultéFacile\r\nCoûtBon marché\r\nIngrédients\r\n6 pêches\r\n20 cl de vin blanc liquoreux\r\n80 g de sucre\r\n1 pincée de clou de girofle en poudre\r\n1 cuillère à  café de cannelle en poudre\r\n1 pincée de gingembre en poudre\r\nÉtapes\r\n1.Pelez les pêches et découpez-les en quartiers.\r\n\r\n2. Versez le vin dans une casserole avec les épices, le sucre et les pêches.\r\n\r\n3. Portez sur feu doux et laissez cuire 15 min.\r\n\r\n4. Laissez refroidir.\r\n\r\n5. Répartissez la soupe dans des coupelles individuelles et servez frais avec des tuiles aux amandes.\r\n\r\n6. Vous pouvez ajouter 1 pincée de poivre moulu au dernier moment.\r\n\r\n7. Décorez de feuilles de menthe fraîche.\r\n\r\nRecette parue dans le numéro 80\r\nLe bon accord vin\r\nCouleur du vin : blanc\r\nAppellation : un jurançon\r\nRégion : Sud-Ouest\r\n\r\nConseils\r\nSur cette base, vous pouvez préparer une soupe de pêches au vin rouge, avec un vin léger, type vin de Loire.\r\nUtilisez les mêmes épices, mais doublez la proportion de sucre. Si vous destinez ce dessert à  des enfants, remplacez le vin par un jus de fruits rouges, maison ou en bouteille.\'', '2018-03-05 10:18:57'),
(7, 2, 'Magrets de canard aux pêches et aux pois gourmands', 'Le magret de canard se marie à merveille avec les fruits. Dans cette recette facile à réaliser, le magret est préparé avec des pêches jaunes et des pois gourmands pour un mariage réussi ! Un peu de miel apporte une touche sucrée savoureuse au plat sucré-salé !\r\nInfos pratiques\r\nNombre de personnes6\r\nTemps de préparation20 minutes\r\nTemps de cuisson25 minutes\r\nDegré de difficultéFacile\r\nCoûtAbordable\r\nIngrédients\r\n3 magrets de canard\r\n6 pêches jaunes\r\n100 g de pois gourmands\r\n1 tablette de bouillon de volaille\r\n60 g de beurre très froid\r\n1 cuil. à café de miel de châtaignier\r\n3 brins de romarin\r\nsel poivre\r\nÉtapes\r\n1. Faites chauffer une poêle à blanc. Quadrillez le gras des magrets avec la pointe d’un couteau. Salez et poivrez, puis posez-les dans la poêle, côté peau dessous.\r\n\r\n2. Faites cuire 15 min à feu doux, en les retournant à mi-cuisson et en retirant la graisse au fur et à mesure.\r\n\r\n3. Pendant ce temps, épluchez et coupez les pêches en quartiers, réservez. Effilez les pois, plongez-les dans de l’eau bouillante salée pour 3 min de cuisson. Égouttez et réservez.\r\n\r\n4. A la fin de la cuisson des magrets, ajoutez le miel dans la poêle ainsi que les pêches et les pois gourmands. Poursuivez la cuisson 5 min en retournant les quartiers de pêches régulièrement.\r\n\r\n5. Retirez les magrets et les pêches de la poêle. Réservez au chaud sous du papier aluminium.\r\n\r\n6. Augmentez le feu sous la poêle et déglacez-la avec 10 cl d’eau chaude et la tablette de bouillon émiettée. Grattez le fond de la poêle avec une spatule en bois. Faites réduire le jus d’un tiers et ajoutez le beurre très froid en parcelles, en fouettant.\r\n\r\n7. Mettez les magrets dans un plat, entourez-les de quartiers de pêches et de pois gourmands, arrosez de sauce et décorez de brins de romarin.\r\n\r\nRecette parue dans le numéro HSC07\r\nLe bon accord vin\r\nCouleur du vin : rouge\r\nAppellation : un pomerol\r\nRégion : Bordeaux\r\n\r\nConseils\r\nDécouvrez quel vin servir avec le magret de canard pour trouver l\'accord idéal avec cette recette.', '2018-03-05 10:24:18'),
(8, 2, 'Rôti de veau aux pêches', 'Twistez le traditionnel rôti de veau du dimanche avec cette version sucrée-salée pleine d\'arômes et de couleurs ! Lentement cuit en cocotte avec des quartiers de pêches, des oignons, du beurre, du citron, des herbes aromatiques et du bouillon, le veau révélera une tendreté et une saveur incomparables.\r\nInfos pratiques\r\nNombre de personnes4\r\nTemps de préparation15 minutes\r\nTemps de cuisson1 h 15 minutes\r\nDegré de difficultéTrès facile\r\nCoûtAbordable\r\nIngrédients\r\n1 kg de rôti de veau\r\n600 g de pêches\r\n50 g de beurre\r\n2 cuil. à soupe d’huile\r\n3 oignons\r\n1 bouquet garni\r\n1 tasse de bouillon\r\n1 citron\r\nsel et poivre\r\nÉtapes\r\n1. Mettez à chauffer doucement l’huile et 20 grammes de beurre dans une cocotte puis mettez la viande à revenir sur feu vif ; ajoutez les oignons coupés en rondelles épaisses. Lorsque la viande et les oignons sont dorés, ajoutez le bouquet garni, salez, poivrez et mouillez avec le bouillon. \r\n\r\n2. Couvrez et laissez cuire 1 h 10 en retournant la viande de temps en temps et en maintenant sur feu moyen. Quelques minutes avant la fin de la cuisson de la viande, égouttez les pêches et faites-les revenir dans le reste de beurre, à la poêle. Une fois dorées, installez-les dans la cocotte. \r\n\r\n3. Arrosez du jus de citron et laissez cuire encore 5 minutes. Servez le rôti de veau découpé en tranches et entouré de pêches. Déglacez la sauce, ôtez le bouquet garni et versez en saucière.\r\n\r\nRecette parue dans le numéro NCF\r\nConseils\r\nAccompagnez cette recette de légumes de saison cuits à l\'anglaise.\r\n\r\nVous avez aimé cette recette? Testez également notre recette de mignon de veau à l\'orange.', '2018-03-05 10:26:57'),
(9, 2, 'Salade de poulet aux pêches', 'salade de poulet, pêches, entrées, côtes-de-provence rosé\r\nInfos pratiques\r\nNombre de personnes6\r\nTemps de préparation30 minutes\r\nTemps de cuisson10 minutes\r\nDegré de difficultéFacile\r\nCoûtBon marché\r\nIngrédients\r\n4 pêches blanches ou jaunes 3 blancs de poulet sans peau 1 gros oignon rouge 1 concombre 4 brins de thym-citron 10 cl d’huile d’olive 1/2 citron fleur de sel poivre du moulin\r\n\r\nÉtapes\r\nCoupez les blancs de poulet en très fines lamelles. Faites chauffer 2 cuillerées à soupe d’huile dans une poêle antiadhésive et faites-y dorer les lamelles de poulet. Quand elles sont bien colorées, baissez le feu et poursuivez la cuisson 5 min en remuant. Salez, poivrez et réservez-les. Pelez les pêches (ébouillantez-les si nécessaire). Coupez-les en tranches fines en éliminant les noyaux. Lavez le concombre et coupez-les en très fins bâtonnets. Effeuillez le thym-citron. Epluchez et émincez l’oignon. Pressez le demi-citron. Dans un saladier, mélangez les lamelles de poulet, les tranches de pêche, les bâtonnets de concombre, l’oignon émincé et le thym-citron. Arrosez de jus de citron et du reste d’huile, salez et poivrez. Mélangez. Répartissez dans des assiettes ou des verres et servez aussitôt.\r\n\r\nRecette parue dans le numéro HSC\r\nLe bon accord vin\r\nCouleur du vin : rosé\r\nAppellation : un côtes-de-provence\r\nRégion : Provence et Corse\r\n\r\nConseils\r\nUn autre parfum : Vous pouvez remplacer le thym-citron, pas toujours facile à trouver, par du romarin réduit en poudre. Sa saveur se marie aussi très bien à cette salade fraîche et raffinée.', '2018-03-05 10:31:29'),
(10, 2, 'Salade de foies de volaille aux pêches', 'Salade de foies de volaille aux pêches, salade de foies, foies de volaille, salade aux pêches, plat\r\nInfos pratiques\r\nNombre de personnes4\r\nTemps de préparation10 minutes\r\nTemps de cuisson5 minutes\r\nDegré de difficultéFacile\r\nCoûtBon marché\r\nIngrédients\r\n4 pêches \r\nhuile d\'olive \r\n1 gousse d\'ail \r\n400 g de foies de volaille \r\n2 cuil. à café d\'origan \r\nsel \r\npoivre \r\nvinaigre \r\n100 de cerneaux de noix \r\n2 pommes \r\n1 frisée\r\n\r\nÉtapes\r\nPelez et dénoyautez 4 pêches, coupez-les en cubes de 1,5 cm de côté. Dans une poêle, faites revenir à l’huile d’olive 5 min 1 gousse d’ail hachée, 400 g de foies de volaille en lanières et 2 cuil. à café d’origan. Salez, poivrez. Dans un saladier, préparez une vinaigrette, ajoutez 100 g de cerneaux de noix concassés grossièrement et les dés de 2 pommes. Ajoutez-y 1 frisée effeuillée, les pêches et les foies de volaille. Mélangez et servez.', '2018-03-05 10:43:09'),
(11, 1, 'Tiramisu aux pêches et au banyuls', 'Le tiramisu en été pourra parfaitement marier le mascarpone aux pêches et au vin de Banyuls. Facile à réaliser, mieux vaut le laisser longuement au frais pour faire un dessert rafraîchissant.\r\nInfos pratiques\r\nNombre de personnes4\r\nTemps de préparation15 minutes\r\nTemps de réfrigération4 heures\r\nCoûtBon marché\r\nIngrédients\r\n3 pêches jaunes ou blanches bien mûres\r\n250 g de mascarpone \r\n16 biscuits à la cuillère \r\n3 jaunes d’œufs \r\n60 g de sucre en poudre\r\n3 cuil. à soupe de vin de Banyuls\r\ncacao en poudre\r\n\r\nÉtapes\r\nLaissez un peu à l’avance le mascarpone à température ambiante pour qu’il s’assouplisse. \r\nPlacez 4 coupes au réfrigérateur.\r\nFouettez les jaunes d’œufs et le sucre dans une jatte jusqu’à ce que le mélange blanchisse, puis incorporez le mascarpone et continuez à fouetter jusqu’à l’obtention d’une crème bien lisse. \r\nRéservez au frais. \r\n\r\nEpluchez les pêches (si nécessaire, ébouillantez-les quelques secondes pour faciliter l’opération) en récupérant le jus dans une assiette creuse contenant le banyuls. \r\nDétaillez la chair en morceaux et mixez-la en purée. \r\nSortez les coupes du réfrigérateur. \r\n\r\nTrempez rapidement 2 biscuits dans le jus au banyuls et disposez-les dans chaque coupe. \r\nRépartissez la purée de pêche, puis 2 autres biscuits trempés. \r\nRecouvrez avec le mélange au mascarpone et placez les tiramisu 4 h au réfrigérateur. \r\n\r\nPour servir, poudrez d’un voile de cacao avec une passoire fine.\r\n\r\nRecette parue dans le numéro HS9\r\nLe bon accord vin\r\nType de vin : Vin doux naturel\r\nCouleur du vin : blanc\r\nAppellation : un banyuls\r\nRégion : Languedoc-Roussillon\r\n\r\n', '2018-03-05 10:49:22'),
(12, 2, 'Carpaccio de Saint-Jacques à la pêche et à la verveine', 'Carpaccio de Saint-Jacques à la pêche parfumée à la verveine\r\nInfos pratiques\r\nNombre de personnes4\r\nTemps de préparation1 h\r\nTemps de réfrigération50 min\r\nDegré de difficultéFacile\r\nCoûtUn peu cher\r\nIngrédients\r\n12 noix de Saint-Jacques \r\n50 g d\'huile d\'olive \r\nfleur de sel \r\n100 g de jus de pêche jaune \r\n3 pêches blanches \r\n1 botte de verveine\r\nÉtapes\r\n1. Taillez les noix de Saint-Jacques en fines tranches. Puis réservez au frais.\r\n\r\n2. Mélangez le jus de pêche et l’huile d’olive, ajouter les feuilles d’une ½ botte de verveine et réservez au frais. \r\nCoupez 2 pêches blanches en fines tranches et une autre en petits cubes. \r\nDans une assiette bien froide, disposez un emporte pièce rond de 15 cm de diamètre et déposez y les tranches de Saint-Jacques, alternées avec les fines tranches de pêches, légèrement chevauchées. \r\n\r\n3. Assaisonnez de vinaigrette à la pêche et parsemez de fleur de sel de Guérande. \r\n\r\n4. Décorez de feuilles de verveine et des morceaux de pêches.', '2018-03-05 10:51:33'),
(13, 3, 'Papillotes de pêches blanches au pain d’épice', 'papillotes de pêches, pain d\'épice, desserts, muscat-de-rivesaltes\r\nInfos pratiques\r\nNombre de personnes4\r\nTemps de préparation25 minutes\r\nTemps de cuisson15 minutes\r\nDegré de difficultéFacile\r\nCoûtBon marché\r\nIngrédients\r\n4 pêches jaunes bien mûres 4 tranches de pain d’épices 4 gousses de vanille 140 g de sucre sucre roux en poudre le jus de 1 citron 40 g de beurre\r\n\r\nÉtapes\r\nPelez les pêches, coupez-les en deux pour enlever le noyau. Faites bouillir 50 cl d’eau avec le jus de citron et 100 g de sucre. Plongez les demi-pêches dans ce sirop et laissez-les pocher 5 min à petit bouillons. Egouttez-les et coupez-les en quartiers. Préchauffez le four à th 6 (180°). Toastez les tranches de pain d’épice, laissez-les refroidir, puis mixez-les pour obtenir une chapelure grossière. Mélangez-la avec le reste de sucre. Fendez les gousses de vanille en deux et grattez les graines noires à l’aide d’un couteau pointu sur cette préparation. Découpez 4 morceaux de papier sulfurisé, répartissez les quartiers de pêche au centre, parsemez du mélange au pain d’épice, ajoutez le beurre en petites noisettes et refermez hermétiquement les papillotes en les repliant plusieurs fois, sans serrer pour que la chaleur circule. Enfournez pour 10 min. Servez les papillotes chaudes ou tièdes, accompagnez-les à volonté d’une crème anglaise ou de boules de glace vanille.\r\n\r\nRecette parue dans le numéro 86\r\nLe bon accord vin\r\nType de vin : Vin doux naturel\r\nCouleur du vin : blanc\r\nAppellation : Un muscat-de-rivesaltes\r\nRégion : Languedoc-Roussillon', '2018-03-05 11:17:46'),
(14, 1, 'Gaspacho à la pêche jaune et pistache', 'Léger, ce dessert aux fruits d\'été est idéal comme recette diététique. Pêches, framboises, oranges et citron vert sont réunis dans ce gaspacho antioxydant ! Les pistaches ajoutent un peu de calories mais sont riches en fibre. Testez ce gaspacho-dessert de toute urgence !\r\nInfos pratiques\r\nNombre de personnes4\r\nTemps de préparation15 min\r\nTemps de cuissonPas de cuisson\r\nDegré de difficultéFacile\r\nCoûtAbordable\r\nIngrédients\r\n5 pêches jaunes \r\n250 g de framboises\r\n3 oranges \r\n1 citron vert \r\n20 g de pistaches décortiquées non salées\r\nÉtapes\r\n1. Coupez le citron et les oranges en deux, pressez-les. Pelez les pêches, dénoyautez-les, coupez-en une en petits dés.\r\n\r\n2. Mixez les 4 autres et mélangez la purée obtenue avec le jus des agrumes et les dés de pêche réservés. Répartissez dans 4 coupes et placez au réfrigérateur.\r\n\r\n3. Mixez les 2/3 des framboises (réservez le reste pour le décor), passez-les à la passoire fine. Concassez les pistaches.\r\n\r\n4. Au moment de servir, versez le jus de framboises en filet sur la mousse de pêche, saupoudrez de pistaches concassées et décorez du reste de framboises.\r\n\r\nConseils\r\nLe gaspacho (ou gazpacho) est à l’origine une soupe espagnole, originaire de Séville, à base de concombre, tomate, oignon et mie de pain, que l’on mange froide, voire glacée. Aujourd’hui, il se décline aussi en version sucrée, comme dans notre recette.\r\nCôté diététique, ce dessert marie des fruits colorés, véritable cocktail d\'antioxydants : du carotène dans les oranges et les pêches jaunes et des polyphénols dans les framboises. La touche parfumée et croustillante des pistaches ajoute des calories, mais surtout des fibres, minéraux et graisses de qualité. N\'hésitez plus à en glisser dans vos recettes !', '2018-03-05 11:19:58'),
(15, 1, 'Rosaces de pêches à l\'anis', 'anis étoilé [badiane], crème liquide, crème anglaise, sucre en poudre, froid, tiède, pêche, dessert, cuisine facile, cuisine rapide, pêches\r\nInfos pratiques\r\nNombre de personnes4\r\nTemps de préparation10 minutes\r\nTemps de cuisson10 minutes\r\nDegré de difficultéFacile\r\nCoûtBon marché\r\nIngrédients\r\n4 grosses pêches\r\n20 cl de crème liquide\r\n30 cl de crème anglaise (surgelée ou en brique)\r\n4 étoiles de badiane\r\n4 cuillères à  café de sucre en poudre\r\nÉtapes\r\n1. Préchauffez le four à th 8 (240°). Faites bouillir la crème liquide, ajoutez les étoiles de badiane, coupez le feu, couvrez et laissez infuser. \r\n\r\n2. Pelez les pêches (ébouillantez-les si nécessaire). \r\n\r\n3. Coupez-les en quartiers en les dénoyautant. Recoupez les quartiers en lamelles. \r\n\r\n4. Rangez les lamelles dans un grand plat à four ou dans 4 plats individuels. \r\n\r\n5. Dans une jatte, mélangez la crème anglaise avec la crème anisée. \r\n\r\n6. Répartissez cette préparation sur les pêches, poudrez de sucre et enfournez pour une dizaine de minutes, jusqu\'à ce que le dessus caramélise légèrement. \r\n\r\nServez tiède ou froid.\r\n\r\nRecette parue dans le numéro 71\r\nLe bon accord vin\r\nType de vin : Vin doux naturel\r\nCouleur du vin : blanc\r\nAppellation : un muscat-de-rivesaltes\r\nRégion : Languedoc-Roussillon', '2018-03-05 11:22:15'),
(16, 1, 'Pêches languedociennes', 'Un dessert coloré, économique et délicieux. Idéal pour les personnes qui font attention à leur régime : sans lactose, sans gluten et avec peu de sucre, c\'est un dessert léger !\r\nInfos pratiques\r\nNombre de personnes6\r\nTemps de préparation25 minutes\r\nTemps de cuisson15 min\r\nTemps de réfrigération2h\r\nDegré de difficultéTrès facile\r\nCoûtBon marché\r\nIngrédients\r\n1,500 kg de belles pêches \r\n150 g de framboises ou de fraises des bois \r\n1/4 de litre de Banyuls \r\n150 g de sucre en poudre\r\n\r\nÉtapes\r\n1. Faites dissoudre le sucre dans 100 g d\'eau chaude.\r\nAjoutez le Banyuls. \r\n\r\n2. Pochez les pêches à l\'eau bouillante (comme pour des tomates) afin de bien retirer la peau. \r\nCoupez les fruits en deux et enlevez les noyaux. \r\n\r\n3. Placez les pêches dans le plat de service, choisi un peu creux. \r\nGarnissez la cavité centrale de chaque demi-fruit avec quelques fruits rouges, nappez avec le sirop au Banyuls et mettez au frais. \r\n\r\n\r\nRecette parue dans le numéro 1971_971\r\nLe bon accord vin\r\nType de vin : Vin doux naturel\r\nCouleur du vin : rouge\r\nAppellation : anjou\r\nRégion : Vallée de la Loire\r\n\r\nConseils\r\nBien que les pêches jaunes soient plus grosses et plus couramment vendues sur les marchés, je vous conseille les belles pêches blanches, un peu nacrées. \r\nElles sont plus parfumées. \r\nLeur peau doit être encore un peu verte, tout au moins près du pédoncule, et délicatement rosée partout ailleurs. \r\nUne peau trop blanche renferme une pêche un peu fade.', '2018-03-05 11:23:50'),
(17, 2, 'Pizza dinde et pêches', 'INGRÉDIENTS\r\n1 pâte à pizza\r\n3 pêches\r\n100 g de blanc de dinde\r\n10 cl de crème fraîche\r\nconcentré de tomate\r\n100 g de gruyère râpé\r\npersil frais\r\nsel, poivre\r\n\r\n 8 Pers.   10 min   30 min    Pas cher   Facile\r\n\r\n\r\nPRÉPARATION\r\nÉTAPE 1\r\nPréchauffez le four à 200°C.\r\n\r\nÉTAPE 2\r\nEtalez la pâte à pizza sur une plaque de four recouverte de papier sulfurisé.\r\n\r\nÉTAPE 3\r\nEtalez le concentré de tomate sur la pâte.\r\n\r\nÉTAPE 4\r\nNettoyez, dénoyautez et coupez les pêches en fines tranches.\r\n\r\nÉTAPE 5\r\nTaillez le blanc de dinde en petits morceaux.\r\n\r\nÉTAPE 6\r\nRépartissez la dinde et le gruyère sur le concentré de tomates.\r\n\r\nÉTAPE 7\r\nAjoutez les tranches de pêche.\r\n\r\nÉTAPE 8\r\nRecouvrez de crème.\r\n\r\nÉTAPE 9\r\nSalez et poivrez.\r\n\r\nÉTAPE 10\r\nEnfournez pendant 20 à 30 minutes.\r\n\r\nÉTAPE 11\r\nServez bien chaud parsemé de persil ciselé.\r\n', '2018-03-05 12:35:05'),
(18, 1, 'Pêches à la meringue', 'Un dessert qui plaira aux enfants et aux adeptes de recettes originales !\r\nInfos pratiques\r\nCoûtAbordable\r\nIngrédients\r\n2 petits-suisses\r\n6 belles pêches\r\n2 blancs d\'oeufs \r\n1 sachet de sucre vanillé \r\n50g de sucre en poudre\r\n10g de beurre\r\n\r\nÉtapes\r\n1. Préchauffez le four à 180°c (th-6).\r\n\r\n2. Rincez et épongez les pêches.\r\n\r\n3. Coupez-les en deux et dénoyautez-les.\r\n\r\n4. Déposez-les sur une plaque de cuisson recouverte de papier sulfurisé beurré.\r\n\r\n5. Garnissez le centre de chaque demi-fruit d\'une miniquenelle de petit-suisse.\r\n\r\n6. Dans un saladier, montez les blancs en neige ferme, en incorporant à la toute fin les 2 sucres.\r\n\r\n7. Mettez la meringue dans une poche à douille et garnissez les demi-pêches d\'un petit dôme.\r\n\r\n8. Glissez la plaque au four et laissez légèrement dorer la meringue.\r\n\r\n9. Servez à la sortie du four.', '2018-03-05 12:37:41');

-- --------------------------------------------------------

--
-- Structure de la table `medias`
--

DROP TABLE IF EXISTS `medias`;
CREATE TABLE IF NOT EXISTS `medias` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `file` varchar(20000) NOT NULL,
  `legend` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `medias`
--

INSERT INTO `medias` (`id`, `file`, `legend`) VALUES
(1, 'soupe-peche.jpg', 'soupe-peche'),
(2, 'peches-roties.jpg', 'peches-roties'),
(3, 'magrets-canard-peches.jpg', 'magrets-canard-pech'),
(4, 'peche.jpg', ''),
(5, 'peche-amorito.jpg', ''),
(6, 'soupio.jpg', ''),
(7, 'rotidepeche.jpg', ''),
(8, 'cvfhs_009-2.jpg', ''),
(9, '44629238.jpg', ''),
(10, 'cvfhs9_096-2.jpg', ''),
(11, 'carpacciopeche.jpg', ''),
(12, 'cvf086_023.jpg', ''),
(13, 'gaspacho-peche-jaune-pistache.jpg', ''),
(14, 'fotolia_84520617_subscription_yearly_xxl1.jpg', ''),
(15, 'fotolia_72298430_subscription_yearly_xl.jpg', ''),
(16, 'i96043-pizza-poulet-et-peches.jpg', ''),
(17, 'peche-meringue.jpg', '');

-- --------------------------------------------------------

--
-- Structure de la table `medias_liaisons`
--

DROP TABLE IF EXISTS `medias_liaisons`;
CREATE TABLE IF NOT EXISTS `medias_liaisons` (
  `id_media` smallint(6) NOT NULL,
  `id_article` smallint(6) NOT NULL,
  KEY `id_article` (`id_article`),
  KEY `id_media` (`id_media`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `medias_liaisons`
--

INSERT INTO `medias_liaisons` (`id_media`, `id_article`) VALUES
(1, 3),
(4, 4),
(5, 5),
(6, 6),
(3, 7),
(7, 8),
(8, 9),
(9, 10),
(10, 11),
(11, 12),
(12, 13),
(13, 14),
(14, 15),
(15, 16),
(16, 17),
(17, 18);

-- --------------------------------------------------------

--
-- Structure de la table `rubriques`
--

DROP TABLE IF EXISTS `rubriques`;
CREATE TABLE IF NOT EXISTS `rubriques` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `titre` varchar(256) NOT NULL,
  `texte` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `rubriques`
--

INSERT INTO `rubriques` (`id`, `titre`, `texte`) VALUES
(1, 'sucré', 'des recettes sucré'),
(2, 'salé', 'des recettes salé'),
(3, 'sucré et salé', 'des recettes salé et sucre'),
(4, 'divers', 'autre');

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `titre` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tags`
--

INSERT INTO `tags` (`id`, `titre`) VALUES
(1, 'Débutant'),
(2, 'Intermédiaire'),
(3, 'Confirmé');

-- --------------------------------------------------------

--
-- Structure de la table `tags_liaisons`
--

DROP TABLE IF EXISTS `tags_liaisons`;
CREATE TABLE IF NOT EXISTS `tags_liaisons` (
  `id_tag` smallint(6) NOT NULL,
  `id_article` smallint(6) NOT NULL,
  KEY `id_article` (`id_article`),
  KEY `id_tag` (`id_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tags_liaisons`
--

INSERT INTO `tags_liaisons` (`id_tag`, `id_article`) VALUES
(3, 14),
(2, 10),
(3, 15),
(2, 12),
(1, 16),
(1, 3),
(1, 17),
(2, 17),
(2, 18),
(1, 4),
(2, 5),
(3, 7),
(2, 6),
(3, 13),
(1, 8),
(2, 8),
(1, 11),
(1, 9);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`id_rub`) REFERENCES `rubriques` (`id`);

--
-- Contraintes pour la table `medias_liaisons`
--
ALTER TABLE `medias_liaisons`
  ADD CONSTRAINT `medias_liaisons_ibfk_1` FOREIGN KEY (`id_article`) REFERENCES `articles` (`id`),
  ADD CONSTRAINT `medias_liaisons_ibfk_2` FOREIGN KEY (`id_media`) REFERENCES `medias` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
