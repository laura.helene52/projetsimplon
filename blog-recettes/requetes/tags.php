<?php

/*
    REQUETE TAG

*/

$order = ORDER_ARTICLES == 'date' ? 'date DESC' : 'titre ASC';

$query = $pdo->prepare('SELECT * FROM tags');
$query->bindParam(':id', $_GET['id'] , PDO::PARAM_INT);
$query->execute();
$tags = $query->fetchAll();
$query->closeCursor();

if( !empty( $_GET['tag']) )
{
  $sql = "
  SELECT * FROM articles
  INNER JOIN `tags_liaisons` ON `tags_liaisons`.id_article = `articles`.id
  WHERE `tags_liaisons`.id_tag = :id_tag
  GROUP BY `articles`.id
  ORDER BY $order
  ";
  $query = $pdo->prepare( $sql );
  $query->bindParam(':id_tag', $_GET['tag'] , PDO::PARAM_INT);
}
else
{
  $sql = "
  SELECT * FROM articles
  INNER JOIN `tags_liaisons` ON `tags_liaisons`.id_article = `articles`.id
  GROUP BY `articles`.id
  ORDER BY $order
  ";
  $query = $pdo->prepare( $sql );
}

$query->execute();
$articles = $query->fetchAll();
$query->closeCursor();
