<?php

/*
    REQUETE RUBRIQUE

    1/ Récupérer les informations de la rubrique dont l'id est dans l'url
    2/ Récupérer les articles classés dans la rubrique

*/

$order = ORDER_ARTICLES == 'date' ? 'date DESC' : 'titre ASC';

$query = $pdo->prepare
(
    "SELECT id,titre,texte
    FROM rubriques
    WHERE id = :id"
);
$query->bindParam(':id', $_GET['id'] , PDO::PARAM_INT);
$query->execute();
$rubrique = $query->fetch(); //1 tableau correspondant à la ligne
$query->closeCursor();

$query = $pdo->prepare
(
    "SELECT id
    FROM articles
    WHERE id_rub = :id_rub
    ORDER BY $order, id DESC"
);
$query->bindParam(':id_rub', $_GET['id'] , PDO::PARAM_INT);
$query->execute();
$articles = $query->fetchAll();
$query->closeCursor();
