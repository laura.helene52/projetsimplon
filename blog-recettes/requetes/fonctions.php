<?php

require_once dirname(__FILE__) . '/../inc/markdown/Markdown.inc.php';
use Michelf\Markdown;

/* ==================================================================================
  Fonction de requête
  Lecture d'une ligne de la table en fonction de l'id

  $query->fetch(); -> retourne la première ligne trouvée
=====================================================================================*/

function getRubrique($id)
{
  global $pdo;
  $query = $pdo->prepare('SELECT * FROM rubriques WHERE id = :id');
  $query->execute(array(':id' => $id));
  return $query->fetch();
}

function getArticle($id)
{
  global $pdo;
  $date_format = FORMATTER_DATE;
  $query = $pdo->prepare("
    SELECT id,titre,texte,id_rub,
    DATE_FORMAT(date, '$date_format') AS date,
    DATE_FORMAT(date, GET_FORMAT(DATETIME, 'ISO') ) AS date_iso
    FROM articles
    WHERE id = :id"
  );
  $query->execute(array(':id' => $id));
  return $query->fetch();
}

function getTitreTag($id)
{
  global $pdo;
  $query = $pdo->prepare('SELECT * FROM tags WHERE id = :id');
  $query->execute(array(':id' => $id));
  $tag = $query->fetch();
  return $tag['titre'];
}


/* ==================================================================================
  bloginfo($type)

  Fonction permettant d'afficher les informations générales du blog
  stockées dans des constantes au moment de l'installation (fichier config/connect)

  $type accepte 2 valeurs :
  'title'       : affiche le titre du blog
  'description' : affiche la description

  <?php bloginfo('title') ?>

=====================================================================================*/
function bloginfo($type)
{
  switch($type)
  {
    case 'title' :
    {
      echo BLOG_TITLE;
      break;
    }
    case 'description' :
    {
      echo BLOG_DESCRIPTION;
      break;
    }
	case 'url' :
    {
      echo BLOG_URL;
      break;
    }
  }
}


function afficheURLTheme()
{
  echo BLOG_URL . '/' . THEME_NAME;
}



/* ==================================================================================
  Informations d'une rubrique dont l'id = $id

  afficheTitreRubrique($id)
  getTexteRubrique($id)
  afficheTexteRubrique($id)

=====================================================================================*/


function afficheTitreRubrique($id)
{
  $rubrique = getRubrique($id);
  echo htmlspecialchars( $rubrique['titre'] );
}

function getTexteRubrique($id)
{
  $rubrique = getRubrique($id);
  return $rubrique['texte'];
}

function afficheTexteRubrique($id)
{
  $rubrique = getRubrique($id);
  echo htmlspecialchars( $rubrique['texte'] );
}

/* ==================================================================================
  Informations d'un article dont l'id = $id
=====================================================================================*/

function get_the_id( $id=NULL )
{
  global $postID;
  if( !isset( $postID ) AND is_array( $id) )
  {
    $postID = $id['id'];
  }
  return $postID;
}

function get_the_title($id)
{
  $article = getArticle($id);
  return $article['titre'];
}

function get_the_content($id)
{
  $article = getArticle($id);
  return $article['texte'];
}

function get_the_date($id)
{
  $article = getArticle($id);
  return $article['date'];
}

function get_the_date_iso($id)
{
  $article = getArticle($id);
  return $article['date_iso'];
}

function get_the_id_rub($id)
{
  $article = getArticle($id);
  return $article['id_rub'];
}

/* =====================================================
	FONCTIONS ACCESSIBLES DANS LA BOUCLE
	OU SUR LA PAGE ARTICLE
  OU AVEC l'id article
========================================================*/

// Renvoie l'id de l'article dans la variable globale $postID;
function the_post()
{
  global $articles;
  global $postID;
  static $i=0;
  $article = $articles[$i];
  $i++;
  $postID = $article['id'];
  if( empty($articles[$i] ) )
    $i=0;
  return $postID;
}

function the_id($id=NULL)
{
  global $postID;
  if( is_array($id) )
  {
    echo $id['id'];
  }
  elseif( empty($id) )
  {
    echo $postID;
  }
  return false;
}

function the_title($id=NULL)
{
  global $postID;
  $title = '';
  if( !empty( $postID ) )
  {
    $title = get_the_title( $postID );
  }
  //Retro compatibilité
  elseif( is_array($id) )
  {
    $title = get_the_title($id['id']);
  }
  elseif( !empty ($id) )
  {
    $title = get_the_title($id);
  }
  else
  {
    $title = get_the_title( $_GET['id'] );
  }
  echo htmlspecialchars( $title);
}

function the_date($id=NULL)
{
  global $postID;
  $date = '';
  if( !empty( $postID ) )
  {
    $date = get_the_date( $postID );
  }
  //Retro compatibilité
  elseif( is_array($id) )
  {
    $date = get_the_date($id['id']);
  }
  echo htmlspecialchars( $date );
}

function the_date_iso($id=NULL)
{
  global $postID;
  $date = '';
  if( !empty( $postID ) )
  {
    $date = get_the_date_iso( $postID );
  }
  //Retro compatibilité
  elseif( is_array($id) )
  {
    $date = get_the_date_iso($id['id']);
  }
  echo htmlspecialchars( $date );
}


function the_content($id=NULL)
{
  global $postID;
  $content = '';
  if( !empty( $postID ) )
  {
    $content = get_the_content( $postID );
  }
  //Retro compatibilité
  elseif( is_array($id) )
  {
    $content = get_the_content($id['id']);
  }
  elseif( !empty ($id) )
  {
    $content = get_the_content($id);
  }
  else
  {
    $content = get_the_content( $_GET['id'] );
  }

  $html = Markdown::defaultTransform(htmlspecialchars( $content ));

  echo $html;
}

function the_url($id=NULL)
{
  global $postID;
  $url = 'article.php?id=';
  if( !empty( $postID ) )
  {
    echo $url . $postID;
  }
  //Retro compatibilité
  elseif( is_array($id) )
  {
    echo $url . $id['id'];
  }
  elseif( !empty ($id) )
  {
    echo $url . $id;
  }
}


/* ==================================================================================
  Couper une chaine

  cutContent( $content, $length )

  $content : la chaine à couper
  $length : le nombre de caractères à conserver en partant du début de la chaine

  Exemples :

  // Affiche les 50 premiers caractères du texte de l'article
  cutContent( $article['texte'], 255 );

  //On récupère le texte de la rubrique sans l'Afficher
  $texteRubrique = getTexteRubrique(2);

  //Affiche les 128 premiers caractères du texte
  cutContent( $texteRubrique, 128 );

=====================================================================================*/

function cutContent( $content, $length ) {

	//Suppression des balises HTML
  $content = Markdown::defaultTransform(htmlspecialchars( $content ));
	$content = strip_tags( $content );

	//Inutile de couper si le nombre de caractères est <= à celui demandé
	if( strlen( $content ) <= $length ) return $content;

	/*
		 Couper la chaine à la longueur demandée
		 	mb_substr effectue une opération de type substr() basée sur le nombre de caractères
	*/
	$content = mb_substr( $content , 0, $length, 'UTF-8' );

	// Récupérer la position du 1er espace en partant de la fin
	$posLastSpace = strrpos( $content, ' ');

	// Récupérer la chaine du début à cette position
	$content = substr( $content , 0, $posLastSpace);
	return $content;
}


/* ==================================================================================
  nav_menu( $args=array() )

  Fonction permettant de générer le menu des rubriques
  Chaque lien est affecté de l'attribut class="menu-link"

  En l'absence d'argument, la fonction renvoie uniquement les liens permettent d'accéder à la page de chaque rubrique

  La fonction admet un tableau associatif facultatif comme argument
  afin de générer automatiquement la structure HTML

  Le tableau d'argument peut contenir 2 valeurs exploitées :

  'container' : Balise de structure globale : div, nav
  'liste' : Présentation du menu dans une liste à puces ou une liste ordonnée : ul ou ol

  Exemples :
  -----------------------------------------------------------------------------

  nav_menu()

  Produira le code html suivant (un lien pour chaque rubrique) :

  <a class="menu-link" href="Lien vers la rubrique">Titre de rubrique</a>
  <a class="menu-link" href="Lien vers la rubrique">Titre de rubrique</a>
  ------------------------------------------------------------------------------

  nav_menu(
    array(
      'container' => 'nav'
    )
  )

  Produira le code html suivant :

  <nav id="menu" class="menu">
    <a class="menu-link" href="Lien vers la rubrique">Titre de rubrique</a>
    <a class="menu-link" href="Lien vers la rubrique">Titre de rubrique</a>
  </nav>

--------------------------------------------------------------------------------
  nav_menu(
    array(
      'liste' => 'ul'
    )
  )

  Produira le code html suivant :

  <ul class="menu-liste">
    <li class="menu-item"><a class="menu-link" href="Lien vers la rubrique">Titre de rubrique</a></li>
    <li class="menu-item"><a class="menu-link" href="Lien vers la rubrique">Titre de rubrique</a></li>
  </ul>

--------------------------------------------------------------------------------
  nav_menu(
    array(
      'container' => 'nav',
      'liste' => 'ul'
    )
  )
  Produira le code html suivant :

  <nav id="menu" class="menu">
    <ul class="menu-liste">
      <li class="menu-item"><a class="menu-link" href="Lien vers la rubrique">Titre de rubrique</a></li>
      <li class="menu-item"><a class="menu-link" href="Lien vers la rubrique">Titre de rubrique</a></li>
    </ul>
  </nav>


=====================================================================================*/

function nav_menu( $args=array() )
{
  global $pdo;
  $query = $pdo->prepare('SELECT * FROM rubriques');
  $query->execute();
  $rubriques = $query->fetchAll();
  $query->closeCursor();

  $container = isset( $args['container'] ) ? '<' . $args['container'] . ' id="menu" class="menu">' . PHP_EOL : '';
  $end_container = isset( $args['container'] ) ? '</' . $args['container'] . '>' . PHP_EOL : '';

  $liste = isset( $args['liste'] ) ? '<' . $args['liste'] . ' class="menu-liste">' . PHP_EOL : '';
  $end_liste = isset( $args['liste'] ) ? '</' . $args['liste'] . '>' . PHP_EOL : '';

  $item = isset( $args['liste'] ) ? "\t" . '<li class="menu-item">' : '';
  $end_item = isset( $args['liste'] ) ? '</li>' . PHP_EOL : PHP_EOL;

  echo $container;
  echo $liste;
  foreach ($rubriques as $rubrique)
  {
    $class_item ='menu-link';
    if( ( is_rubrique() && $_GET['id'] == $rubrique['id'] ) || ( is_article() && get_the_id_rub( $_GET['id'] ) == $rubrique['id'] ) )
    {
      $class_item .= ' active';
    }
    echo $item;
    echo '<a class="' . $class_item . '" href="rubrique.php?id=' . $rubrique['id'] . '">' . $rubrique['titre']. '</a>';
    echo $end_item;
  }
  echo $end_liste;
  echo $end_container;
}

/* ==================================================================================
  medias( $article )

  Fonction permettant de récupérer un tableau de tous les médias liés à l'article
  Ce tableau pourra être lu avec une boucle : chaque ligne est un tableau associatif correspondant à la table medias


=====================================================================================*/

function medias($article=NULL)
{
  global $pdo;
  global $postID;

  $query = $pdo->prepare
  (
    "SELECT * FROM `medias`
    INNER JOIN `medias_liaisons` ON `medias_liaisons`.id_media = `medias`.id
    WHERE `medias_liaisons`.id_article = :id"
  );

  $id = isset( $postID ) ? $postID : $article['id'];

  $query->bindParam(':id', $id , PDO::PARAM_INT);
  $query->execute();
  $medias = $query->fetchAll();
  $query->closeCursor();

  return $medias;
}

/* ==================================================================================
  afficheMedias($article, $args=array())

  Fonction permettant d'afficher les médias liés à un article

  En l'absence d'argument, la fonction renvoie uniquement un balise img par média

  La fonction admet un tableau associatif facultatif comme argument
  afin de générer automatiquement la structure HTML

  Le tableau d'argument peut contenir 2 valeurs exploitées :

  'container' : Balise de structure globale : div, figure, li
  'legend' : p, figcaption affichant le texte de légende

Exemples :

afficheMedias($article)

produira, pour chaque média :

  <img src="medias/file" alt="legend">


afficheMedias($article, $args=array(
  'container' => 'figure',
  'legend' => 'figcaption'
))

produira, pour chaque média :

<figure id="media-id">
  <img src="medias/file" alt="">
  <figcaption>legend</figcaption>
</figure>

=====================================================================================*/

function afficheMedias($article='', $args=array())
{
  $medias = medias($article);
  $end_container = isset( $args['container'] ) ? '</' . $args['container'] . '>' . PHP_EOL : '';

  foreach ($medias as $media)
  {
    $container = isset( $args['container'] ) ? '<' . $args['container'] . ' id="media-' . $media['id'] . '" class="media-article">' . PHP_EOL : '';
    $legend = isset( $args['legend'] ) ? '<' . $args['legend'] . '>' . $media['legend'] . '</' . $args['legend'] . '>' . PHP_EOL : '';
    $alt = isset( $args['legend'] ) ? '' : $media['legend'];
    echo $container;
    echo $legend;
    echo '<img src="medias/' . $media['file'] . '" alt="'. $alt .'">';
    echo $end_container;
  }
}


/* ==================================================================================
  tags( $article )

  Fonction permettant de récupérer un tableau de tous les tags liés à l'article

=====================================================================================*/

function tags($article=NULL)
{
  global $pdo;
  global $postID;

  $query = $pdo->prepare
  (
    "SELECT * FROM `tags`
    INNER JOIN `tags_liaisons` ON `tags_liaisons`.id_tag = `tags`.id
    WHERE `tags_liaisons`.id_article = :id"
  );

  $id = isset( $postID ) ? $postID : $article['id'];

  $query->bindParam(':id', $id , PDO::PARAM_INT);
  $query->execute();
  $tags = $query->fetchAll();
  $query->closeCursor();

  return $tags;
}

function afficheTags($article='')
{
  $tags = tags($article);
  foreach ($tags as $tag)
  {
    $class = ( array_key_exists('tag', $_GET) && $_GET['tag'] == $tag['id'] ) ? 'tag active' : 'tag';
    echo '<a class="'. $class . '" href="tag.php?tag='. $tag['id'] .'">' . $tag['titre'] . '</a>';
  }
}

function afficheClassTags($article='')
{
  $tags = tags($article);
  $class = '';
  foreach ($tags as $tag)
  {
    $class .= $tag['titre'] . ' ';
  }
  echo $class;
}


/* ========================================================================
						FONCTIONS DE TESTS
=========================================================================== */

function is_home()
{
	$contexte = pathinfo($_SERVER['SCRIPT_NAME']);
	if( $contexte['filename'] == 'index') return true;
}

function is_rubrique()
{
	$contexte = pathinfo($_SERVER['SCRIPT_NAME']);
	if( $contexte['filename'] == 'rubrique') return true;
}

function is_tag()
{
	$contexte = pathinfo($_SERVER['SCRIPT_NAME']);
	if( $contexte['filename'] == 'tag') return true;
}

function is_tags()
{
	$contexte = pathinfo($_SERVER['SCRIPT_NAME']);
	if( $contexte['filename'] == 'tags') return true;
}

function is_article()
{
	$contexte = pathinfo($_SERVER['SCRIPT_NAME']);
	if( $contexte['filename'] == 'article') return true;
}

function is_404()
{
	$contexte = pathinfo($_SERVER['SCRIPT_NAME']);
	if( $contexte['filename'] == '404') return true;
}
