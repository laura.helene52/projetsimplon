<?php

include_once('../config/connect.php');
include_once('fonctions.php');

if( !empty( $_GET['tag'] ) )
{
  $sql = "
  SELECT * FROM articles
  INNER JOIN `tags_liaisons` ON `tags_liaisons`.id_article = `articles`.id
  WHERE `tags_liaisons`.id_tag = ?
  GROUP BY `articles`.id
  ";
  $query = $pdo->prepare( $sql );
  $query->execute( array(intval($_GET['tag'])) );
}
else
{
  $sql = "
  SELECT * FROM articles
  INNER JOIN `tags_liaisons` ON `tags_liaisons`.id_article = `articles`.id
  GROUP BY `articles`.id
  ";
  $query = $pdo->prepare( $sql );
  $query->execute();
}


$articles = $query->fetchAll();
$query->closeCursor();


include( '../' . THEME_NAME . '/include/articles-ajax-tag.php');
