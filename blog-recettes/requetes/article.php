<?php

/*
    REQUETE ARTICLE

    recuperer l'article dont l'id est dans l'url
*/

$query = $pdo->prepare
(
    'SELECT id
    FROM articles
    WHERE id = :id'
);
$query->bindParam(':id', $_GET['id'] , PDO::PARAM_INT);
$query->execute();
$articles = $query->fetchAll();
$query->closeCursor();
