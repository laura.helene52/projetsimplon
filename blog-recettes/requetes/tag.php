<?php

/*
    REQUETE TAG

*/

$order = ORDER_ARTICLES == 'date' ? 'date DESC' : 'titre ASC';

$query = $pdo->prepare
(
    'SELECT id
    FROM tags
    WHERE id = :id'
);
$query->bindParam(':id', $_GET['tag'] , PDO::PARAM_INT);
$query->execute();
$tag = $query->fetch();
$query->closeCursor();


$query = $pdo->prepare
(
  "SELECT * FROM articles
  INNER JOIN `tags_liaisons` ON `tags_liaisons`.id_article = `articles`.id
  WHERE `tags_liaisons`.id_tag = :id"
);

$query->bindParam(':id', $_GET['tag'], PDO::PARAM_INT);
$query->execute();
$articles = $query->fetchAll();
$query->closeCursor();
