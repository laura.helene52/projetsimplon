<?php

/*
    REQUETE INDEX

    Récupérer les NB_ARTICLES articles classés par ORDER_ARTICLES

    NB_ARTICLES et ORDER_ARTICLES sont des constantes de configuration -> config/connect.php

*/

//Stockage de la valeur des constante dans des variables pour pouvoir les passer dans la requête
$nombre = NB_ARTICLES;
$order = ORDER_ARTICLES == 'date' ? 'date DESC' : 'titre ASC';

// Utilisation des séparateurs "" afin de pouvoir interpréter les variables à l'intérieur de la chaine de requête
$query = $pdo->prepare("SELECT id FROM articles ORDER BY $order, id DESC LIMIT :nombre");

//Passage du paramètre LIMIT (Limiter le nombre )
$query->bindParam(':nombre', $nombre , PDO::PARAM_INT);

//Exécution de la requête
$query->execute();

//Résultats stockées dans $articles : un tableau dont chaque ligne contient 1 article;
$articles = $query->fetchAll();

//Fermeture de la requête (ré-initialisation du curseur)
$query->closeCursor();
