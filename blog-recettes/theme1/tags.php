<?php include('include/header.php') ?>
  <nav class="menu banner">
    <a class="menu-link"> <?php bloginfo('title'); ?></a>
    <a href="<?php bloginfo('url') ?>"class="menu-link ">Accueil</a>
    <?php nav_menu(); ?>
    <a class="menu-link active" href="tags.php">Tags</a>
    <a class="menu-link " href="<?php bloginfo('url'); ?>/admin/">Admin</a>
  </nav>
  </main>
  <!--
  <section class="bg">
    <div class="container">
      <h2>Ajax</h2>
      <?php if( !empty($tags) ) : ?>
      <nav class="menu-tag" id="ajax-nav">
        <?php if(!isset($_GET['tag'])):?>
        <a class="tags active" href="tags.php">Tous</a>
        <?php else: ?>
          <a class="tags" href="tags.php">Tous</a>
        <?php endif;?>
        <?php foreach ($tags as $tag):?>
          <?php if(isset($_GET['tag'])AND$_GET['tag'] == $tag['id']):?>
        <a class="tags active" href="tags.php?tag=<?php echo $tag['id']?>" ><?php echo strip_tags($tag['titre'])?></a>
        <?php else: ?>
          <a class="tags" href="tags.php?tag=<?php echo $tag['id']?>" ><?php echo strip_tags($tag['titre'])?></a>
        <?php endif;?>
        <?php endforeach;?>
      </nav>
      <?php endif; ?>


      <div id="ajax-container" class="tags-conainer">
      <?php include('include/articles-ajax-tag.php')?>
      </div>
    </div>
  </section>
-->

  <section class="bg">
    <div class="container">
      <h2>Isotope</h2>
      <?php if (!empty($tags)):?>
      <nav class="menu-tag" id="isotope-nav">
        <a class="tags active" href="#">Tous</a>
        <?php foreach ($tags as $tag):?>
        <a class="tags" href="#" data-isotope="<?php echo strip_tags($tag['titre'])?>"><?php echo strip_tags($tag['titre'])?></a>
        <?php endforeach;?>
      </nav>
      <?php endif;?>

        <div id="isotope-container" class="tags-conainer">
          <?php include('include/articles-ajax-tag.php')?>
        </div>

    </div>
  </section>
    <?php include('include/footer.php') ?>
    </main>
  </body>
</html>
