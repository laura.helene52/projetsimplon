<?php include('include/header.php') ?>
<main>
  <nav class="menu banner">
    <a class="menu-link"> <?php bloginfo('title'); ?></a>
    <a href="<?php bloginfo('url') ?>"class="menu-link ">Accueil</a>
    <?php nav_menu(); ?>
    <a class="menu-link" href="tags.php">Tags</a>
    <a class="menu-link" href="<?php bloginfo('url'); ?>/admin/">admin</a>
  </nav>


  <div class="rub">
    <div class="container">
      <h1><?php  afficheTitreRubrique($_GET["id"]) ; ?></h1>
      <p><?php  afficheTexteRubrique($_GET["id"]) ; ?></p>

    </div>
  </div>

  <section class="section-home">
    <div class="container">
      <div class="debutant">

        <div class="ulmes3cards">
          <?php foreach( $articles as $article ) :the_post() ?>
          <article class="card">
            <div class="card-image">
              <?php  afficheMedias() ?>
              <p><?php  afficheTags(); ?></p>
            </div>
            <div class="card-body">
              <h3><?php the_title() ?></h3>
              <p class="date">Publié le<?php the_date() ?></p>
              <a href="article.php?id=<?php the_id(); ?>">Lire la suite</a>
            </div>
          </article>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </section>
</main>
<?php include('include/footer.php') ?>
