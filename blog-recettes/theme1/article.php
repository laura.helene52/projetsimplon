<?php include('include/header.php') ?>


  <nav class="menu banner">
    <a class="menu-link"> <?php bloginfo('title'); ?></a>
    <a href="<?php bloginfo('url') ?>"class="menu-link ">Accueil</a>
    <?php nav_menu(); ?>
    <a class="menu-link active" href="tags.php">Tags</a>
    <a class="menu-link " href="<?php bloginfo('url'); ?>/admin/">Admin</a>
  </nav>

    <main>
      <section>
        <div class="container">
          <div class="debutant">
            <div class="container-article">
              <?php foreach( $articles as $article ) :the_post() ?>
              <article>
                <div>
                  <?php  afficheMedias() ?>
                  <p><?php  afficheTags(); ?></p>
                </div>
                <div>
                  <h1 class="hchart"><?php the_title() ?></h1>
                  <p class="date">Publié le<?php the_date() ?></p>
                  <div class="content-articles">
                    <p><?php the_content() ?></p>
                  </div>
                </div>
              </article>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </section>
    <?php include('include/footer.php') ?>
    </main>
  </body>
</html>
