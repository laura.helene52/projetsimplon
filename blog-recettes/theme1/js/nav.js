'use strict';

document.addEventListener('DOMContentLoaded', function (){

	var banner;
	var position;

	/* On cible la bannière par une méthode de sélection*/
	banner = document.querySelector('.banner');

	//console.log(banner);

	/* On ajoute à la bannière la classe permetttant de la fixer */
	banner.classList.add('home');

	window.addEventListener('scroll', function(){

		position = window.scrollY;

		console.log(position); // Elle varie sans arrêt lors du scroll

		if( position >= 300 )
		{
			banner.classList.add('banner-changed');
		}
		else
		{
			banner.classList.remove('banner-changed');
		}

	});

});
