'use strict';

document.addEventListener('DOMContentLoaded', function() {

  const fields = document.querySelectorAll('.form-field');
  const invalidField = document.querySelector('.error .form-field');

  if( invalidField )
    invalidField.autofocus = true;

  fields.forEach( function( field ) {

    const label = field.previousElementSibling;

    if( field.value.length == 0 )
    {
      label.classList.add('label');
    }
    else
    {
      label.classList.add('label-focus');
      field.classList.add('valid');
    }

    field.addEventListener('focus', function()
    {
      label.classList.add('label-focus');
    });

    field.addEventListener('blur', function()
    {
      if( field.value.length == 0 ) {
        label.classList.remove('label-focus');
      }
      else {
        this.classList.add('valid');
      }
    });

  } );

});
