'use strict';

$(function()
{

  var ajaxNav,
      ajaxNavLinkActive,
      ajaxContainer,
      ajaxDataAttribut,
      url;

  ajaxNav = '#ajax-nav'; // nav
  ajaxNavLinkActive = '#ajax-nav .active';
  ajaxContainer = '#ajax-container'; // Le container dont le contenu est rechargé
  ajaxDataAttribut = 'ajax'; // $('a').data('ajax') = valeur de l'attribut data-ajax d'une balise <a> ;

  url = window.location.pathname; // L'url de la page (sans les variables d'url ni les ancres )


  /*
    FONCTION DE CHARGEMENT AXAX
      1/ idtag contient l'id (clé primaire en base) du tag
          stocké dans l'attribut data-ajax des liens

      2/ la requête ajax est stocké dans le fichier requetes/ajax-tag.php
          lorsqu'on clique sur un lien, javascript transmet cette requête au serveur

            En fonction de la valeur du tag, le serveur renvoi la réponse :

              - tous les articles associés à ce tag
              - ou tous les articles associés à un tag, si idtag est vide

      Déroulement :

      1/ Construction du lien pour transmettre la requête en fonction de idtag
      2/ Remplacement de tout le contenu du bloc ajax par une animation d'attente
      3/ Exécution d'un fonction qui attend 0.5s avant d'exécuter la requête
          Raison ergonomque : cela permet à l'utilisateur de comprendre que quelque chose va changer (l'animation dure donc 0.5s)

      4/ Envoi de la requête au serveur
      5 / Insertion de la réponse à l'intérieur du bloc ajax


  */
  function ajaxLoad(idtag)
  {
    var ajaxRequest;
    ajaxRequest = './requetes/ajax-tag.php?tag=' + idtag;
    $(ajaxContainer).html('<div class="loader">Loading...</div>');
    window.setTimeout( function() {
      $.get( ajaxRequest, function( response ) {
        $(ajaxContainer).html( response );
      });
    }, 500);
  }


  /*
    FONCTION LIEE À L'EVENEMENT lorsqu'on clique sur un lien de la nav

      1/ On récupère la valeur de l'attribut data-ajax du lien cliqué, qui contient l'id du tag en base
      2/ On supprime la classe active sur les liens qui l'on
      3/ On ajoute la class active sur le lien cliqué
      4/ On lance la fonction de requête

      5/ On stocke cette requête dans l'historique du navigateur

  */

  function onClickExecute(e)
  {
    e.preventDefault();

    var idtag;
    idtag = $(this).data(ajaxDataAttribut);

    if( idtag == undefined )
      idtag ='';

    $(ajaxNavLinkActive).removeClass('active');
    $(this).addClass('active');

    ajaxLoad(idtag);

    window.history.pushState({tag : idtag},'', url + '?tag=' + idtag);
  }


  // Installation du gestionnaire d'événement sur les liens de la nav
  $(ajaxNav).on('click', 'a' ,onClickExecute);


  // Gestion de l'historique du navaigateur

  window.onpopstate = function(e) {
    var idtag;
    idtag ='';
    $(ajaxNavLinkActive).removeClass('active');
    if( e.state !== null )
    {
      idtag = e.state.tag;
      $('[data-ajax="' + idtag + '"]').addClass('active');
    }
    else
    {
      $('a', ajaxNav ).not('[data-ajax]').addClass('active');
    }
    ajaxLoad(idtag);
  };
});
