<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

    <?php if(is_home()) : ?>

        <title><?php bloginfo('title');?></title>
        <meta name="description" content="<?php bloginfo('description');?>">

    <?php elseif (is_article()): ?>

        <title><?php the_title($_GET['id'])?></title>
        <meta name="description" content="<?php echo cutContent(get_the_content($_GET['id'] ) , 150 )?>">

    <?php elseif (is_rubrique()): ?>

        <title><?php afficheTitreRubrique( $_GET['id'])?></title>
        <meta name="description" content="<?php echo cutContent( getTexteRubrique( $_GET['id'] ) , 150 ) ?>">

    <?php elseif( is_tag() ) : ?>

        <title><?php echo getTitreTag( $_GET['id'] ) ?></title>

    <?php elseif( is_tags() ) : ?>

          <title>Tags</title>

    <?php elseif( is_404() ) : ?>

          <title>404 : <?php bloginfo('title') ?></title>


    <?php endif; ?>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Prata%7CTitillium+Web" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
        <link rel="stylesheet" href="<?php afficheURLTheme();?>/css/styles1.css">
    </head>
    <body>
