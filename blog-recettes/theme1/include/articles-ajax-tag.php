<div class="ulmes3cards2">
  <?php foreach( $articles as $article ) :the_post() ?>
  <article class="card <?php afficheClassTags() ?>">
    <div class="card-image">
      <?php  afficheMedias() ?>
      <p><?php  afficheTags(); ?></p>
    </div>
    <div class="card-body">
      <h3><?php the_title() ?></h3>
      <p class="date">Publié le<?php the_date() ?></p>
      <a href="article.php?id=<?php the_id(); ?>">Lire la suite</a>
    </div>
  </article>
  <?php endforeach; ?>
</div>
