<?php include('include/header.php') ?>
  <nav class="menu banner">
    <a class="menu-link"> <?php bloginfo('title'); ?></a>
    <a href="<?php bloginfo('url') ?>"class="menu-link ">Accueil</a>
    <?php nav_menu(); ?>
    <a class="menu-link active" href="tags.php">Tags</a>
    <a class="menu-link " href="<?php bloginfo('url'); ?>/admin/">Admin</a>
  </nav>
  <section>
    <div class="container">
      <div class="debutant">
        <div class="ulmes3cards2">
          <?php foreach( $articles as $article ) :the_post() ?>
          <article class="card">
            <div class="card-image">
              <?php  afficheMedias() ?>
              <p><?php  afficheTags(); ?></p>
            </div>
            <div class="card-body">
              <h3><?php the_title() ?></h3>
              <p class="date">Publié le<?php the_date() ?></p>
              <a href="article.php?id=<?php the_id(); ?>">Lire la suite</a>
            </div>
          </article>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </section>
    <?php include('include/footer.php') ?>
    </main>
  </body>
</html>
