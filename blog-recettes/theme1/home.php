<?php include('include/header.php') ?>
<header>
  <div class="logo"></div>
    <nav class="menu banner">



      <?php if(is_home()) : ?>

      <a href="<?php bloginfo('url') ?>" class="menu-link active">Accueil</a>

      <?php else : ?>

      <a href="<?php bloginfo('url') ?>" class="menu-link ">Accueil</a>

      <?php endif ; ?>

      <?php nav_menu(); ?>
      <a class="menu-link " href="tags.php">Tags</a>

    </nav>

</header>
  <main>

    <section id="about" class="about split-container">
      <div class="container">
        <div class="col1">
          <div class="text">
            <h2>La pêche dans tout ces états</h2>
            <p class="prep">Découvre vite toutes nos recettes à la pêche, sucrées et salées !</p>
            <p>Qu’elle soit jaune ou blanche, la pêche sublime nos recettes estivales
              qu’elles soient salées et sucrées. Sa chair juteuse et parfumée s’incorpore
               dans beaucoup de nos plats et nos desserts de saison colorés, légers et
               rafraîchissants à souhait. Oui, faites de la pêche le fruit star de vos
               assiettes durant l’été. Savourez-la en toute occasion avec ces 15 recettes
               salées et sucrées à la pêche présentées ici même dans cette sélection.</p>
               <p>Découvrez donc la salade de pêches au jambon de Parme et à la mozzarella,, le flan aux pêches au mascarpone ou encore le gâteau de crêpes aux pêches, à la chantilly et à la noix de coco. Vous en voulez encore ? Pas de souci, jetez aussi un oeil aux toasts à la feta et aux pêches, à la pizza à la dinde et aux pêches ou encore au couscous végétarien aux pêches et aux légumes, par exemple.</p>
          </div>
          <div class="mac">

          </div>
        </div>
       </div>
    </section>




    <section class="section-home">
        <div class="container">
          <div class="debutant">
            <h2>Actualité Gourmande</h2>

            <div class="ulmes3cards">
              <?php foreach( $articles as $article ) :the_post() ?>
              <article class="card">
                <div class="card-image">
                <?php  afficheMedias(); ?>
                <p><?php  afficheTags(); ?></p>
                </div>
                <div class="card-body">
                  <p class="date">Publié le <?php the_date() ?></p>
                  <h3><?php the_title() ?></h3>

                  <a href="article.php?id=<?php the_id(); ?>">Lire la suite</a>
                </div>
              </article>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </section>




      <section id="home" class="competence anime-slide-in split-container">
				<div class="container">
					<div class="case">
						<h2>Toutes les recettes classées par niveau</h2>
            <p>Que vous soyer un cuisinier du dimanche ou un expert chevrauné il y'a forcement la recette que vous voulez ici et en plus classé pas niveau de difficulté!</p>
            <p>Alors trouve vite la recette qu'il te faut et épaté tes convive!</p>
            <div class="forme">
  			       <div class="niveaux pe">Débutant</div>
               <div class="niveaux">Intermédiaire</div>
               <div class="niveaux po">Confirmé</div>
            </div>
          <!--  <div class="forme">
              <h3>Débutant</h3>
              <h3>Intermédiaire</h3>
              <h3>Confirmé</h3>
            </div> -->
					</div>
				</div>
			</section>


      <section class="photoepche">
        <div class="fin">
          <h4>La pêche, un fruit délicieux, pelin de vitamine et surtoutimmanquable <br> dans vos recettes de cette été!</h4>
      
        </div>
      </section>

    </main>
    <?php include('include/footer.php') ?>
    </body>
  </html>
