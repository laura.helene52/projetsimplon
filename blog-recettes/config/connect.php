<?php
/*==============================================================================
                    CONSTANTES DE CONGIGURATION
===============================================================================*/

  //Chemin absolu du répertoire du blog
  define('BLOG_URL', '/blog-recettes');

  //Titre du blog
  define('BLOG_TITLE','Peche et compagnies');
  //Slogan, description
  define('BLOG_DESCRIPTION','les peches donne la peche !');

  //Nombre d'articles à afficher sur la page d'accueil
  define('NB_ARTICLES',6);

  /*
    Champ déterminant l'ordre d'affichage des articles
      'titre' -> par ordre alphabétique
      'date'  -> par date chronologique inversée (le dernier écrit en premier)
  */
  define('ORDER_ARTICLES','date');

  //Format de date
  define('FORMATTER_DATE', '%W %e %M %Y');

  // Accès à la base de données
  define('SQL_HOST', 'localhost'); // Serveur de la bse de données (peut être différent du nom de domaine du blog )
	define('SQL_DB', 'blog_1'); // Nom de la base
	define('SQL_USER', 'bloguser'); // Identifiant de l'utilisateur de la base
	define('SQL_PASS', '123456'); // Mot de passe de l'utilisateur de la base

  define('THEME_NAME','theme1');

/* =============================================================================
                  NE RIEN MODIFIER A PARTIR DE LA
===============================================================================*/


  // Afficher toutes les erreurs : à commenter en production
  ini_set('display_errors', 1);
  error_reporting(E_ALL);

	try
	{
	// On se connecte à MySQL : données de connexion au serveur, puis à la base, puis utilisateur + mot de passe
		$pdo = new PDO('mysql:host=' . SQL_HOST . ';dbname='.SQL_DB, SQL_USER, SQL_PASS);
	}
	catch (Exception $e)
	{
	// En cas d'erreur, on affiche un message et on arrête tout (page plantée)
		die('Erreur : ' . $e->getMessage());
	}

  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	// Etablir la communication entre PHP et Mysql en UTF-8
	$pdo->query("SET NAMES 'utf8'");
	// Format de date en français
	$pdo->query("SET lc_time_names = 'fr_FR'");
