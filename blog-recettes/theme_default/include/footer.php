      <footer class="page-footer">
        <p class="container">@Veronique CUOMO - TP WEBDESIGNER session 2017/2018 - Ce petit applicatif a une vocation pédagogique et ne doit pas être exploité en production</p>
      </footer>
      <?php if(is_tags()) : ?>
        <script src="<?php afficheURLTheme();?>/js/vendor/jquery-3.2.1.min.js"></script>
        <script src="<?php afficheURLTheme();?>/js/ajax-tag.js"></script>
        <script src="<?php afficheURLTheme();?>/js/isotope.js"></script>
      <?php endif; ?>
      <script src="<?php afficheURLTheme();?>/js/main.js"></script>
    </body>
</html>
