<?php if( !empty($articles) ) : ?>
  <div class="flex">
  <?php foreach( $articles as $article ) : the_post() ?>
    <article id="post-<?php the_id(); ?>" class="hentry col-2 <?php afficheClassTags() ?>" itemscope itemtype="http://schema.org/Article">
      <div class="article-media thumbnail">
        <?php afficheMedias(); ?>
      </div>
      <header class="article-header" itemscope itemtype="http://schema.org/Recipe">
        <h3 itemprop="name" class="hentry-title"><?php the_title() ?></h3>
        <div class="meta">
            <p>Posté le
            <time class="published" itemprop="dateCreated" datetime="<?php the_date_iso() ?>"><?php the_date() ?></time>
            dans la rubrique <strong itemprop="about"><?php afficheTitreRubrique( get_the_id_rub( get_the_id() ) ) ?></strong></p>
            <p><?php afficheTags(); ?></p>
        </div>
      </header>
      <div class="entry-summary" itemprop="articleBody">
        <?php echo cutContent( get_the_content( get_the_id() ) , 50 ) ?>
      </div>
      <a href="<?php the_url() ?>" itemprop="url" rel="bookmark">Lire la suite</a>
    </article>
  <?php endforeach; ?>
  </div>
<?php endif; ?>
