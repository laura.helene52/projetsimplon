<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

		<?php if( is_home() ) : ?>

        <title><?php bloginfo('title') ?></title>
        <meta name="description" content="<?php bloginfo('description') ?>">

		<?php elseif( is_article() ) : ?>

		    <title><?php the_title( $_GET['id'] ) ?></title>
        <meta name="description" content="<?php echo cutContent( get_the_content( $_GET['id'] ) , 150 ) ?>">

		<?php elseif( is_rubrique() ) : ?>

		    <title><?php afficheTitreRubrique( $_GET['id'] ) ?></title>
        <meta name="description" content="<?php echo cutContent( getTexteRubrique( $_GET['id'] ) , 150 ) ?>">

    <?php elseif( is_tag() ) : ?>

        <title><?php echo getTitreTag( $_GET['tag'] ) ?></title>

    <?php elseif( is_tags() ) : ?>

          <title>Tags</title>

    <?php elseif( is_404() ) : ?>

      <title>404 : <?php bloginfo('title') ?></title>

	   <?php endif; ?>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php afficheURLTheme();?>/css/normalize.css">
        <link rel="stylesheet" href="<?php afficheURLTheme();?>/css/main.css">

    </head>
    <body role="document" itemscope="itemscope" itemtype="http://schema.org/WebPage">
      <header class="banner home">
        <div class="container banner-container">
          <a class="logo" href="<?php bloginfo('url') ?>" title="Retour à l'accueil">
            <?php bloginfo('title') ?>
          </a>
          <button class="button-burger" type="button" data-toggle="menu" aria-label="Ouvrir/Fermer navigation"></button>
    			<nav id="menu" class="menu">


            <a class="menu-link<?php if( is_home() ) : ?> active<?php endif; ?>" href="<?php bloginfo('url') ?>">Accueil</a>           

    				<?php nav_menu(); ?>

            <?php if( is_tags() ) : ?>
            <a class="menu-link active" href="tags.php">Tags</a>
            <?php else : ?>
            <a class="menu-link" href="tags.php">Tags</a>
            <?php endif; ?>

            <a class="menu-link" href="<?php bloginfo('url') ?>/admin/">Administration</a>
    			</nav>
        </div>
      </header>
