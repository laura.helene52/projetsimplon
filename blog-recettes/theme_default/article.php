<?php include('include/header.php') ?>

<?php foreach( $articles as $article ) : the_post() ?>
<main itemprop="mainContentOfPage" class="site-main">
	<nav class="breadcrumb" aria-label="breadcrumb">
		<ol class="container" itemscope itemtype="http://schema.org/BreadcrumbList">
			<li
				itemscope
				itemprop="itemListElement" itemtype="http://schema.org/ListItem">
					<a itemprop="item" href="<?php bloginfo('url') ?>">Accueil</a>
					<meta itemprop="position" content="1">
			</li>
		 	<li
				itemscope
				itemprop="itemListElement" itemtype="http://schema.org/ListItem">
					<a href="" itemprop="item">><?php afficheTitreRubrique( get_the_id_rub( get_the_id() ) ) ?></a>
					<meta itemprop="position" content="2">
				</li>
				<li
					itemscope
					itemprop="itemListElement" itemtype="http://schema.org/ListItem"
					aria-current="page">
						<strong itemprop="item"><?php the_title() ?></strong>
						<meta itemprop="position" content="3">
					</li>
	 	</ol>
	</nav>
	<article id="post-<?php the_id(); ?>" class="hentry clearfix" itemscope itemtype="http://schema.org/Recipe">
		<div class="container">
			<div class="flex">
					<header class="col-2 p-3">
							<h1  itemprop="name" class="hentry-title"><?php the_title() ?></h1>
							<p class="meta">
									Posté le
									<time class="published" itemprop="dateCreated" datetime="<?php the_date_iso() ?>"><?php the_date() ?></time>
									dans la rubrique <span itemprop="about"><?php afficheTitreRubrique( get_the_id_rub( get_the_id() ) ) ?></span>
							</p>
							<div class="article-media">
								<?php afficheMedias(); ?>
							</div>
							<p><?php afficheTags(); ?></p>
					</header>

					<div itemprop="articleBody" class="entry-content p-3 col-2">
						<?php the_content() ?>
					</div>

				</div>
		</div>
	</article>
</main>
<?php endforeach; ?>
<?php include('include/footer.php') ?>
