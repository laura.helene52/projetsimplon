<?php include('include/header.php') ?>
<main itemprop="mainContentOfPage" class="site-main">
	<nav class="breadcrumb" aria-label="breadcrumb">
		<ol class="container" itemscope itemtype="http://schema.org/BreadcrumbList">
			<li
				itemscope
				itemprop="itemListElement" itemtype="http://schema.org/ListItem">
					<a itemprop="item" href="<?php bloginfo('url') ?>">Accueil</a>
					<meta itemprop="position" content="1">
			</li>
		 	<li
				itemscope
				itemprop="itemListElement" itemtype="http://schema.org/ListItem"
				aria-current="page">
					<strong itemprop="item"><?php AfficheTitreRubrique( $_GET['id']) ?></strong>
					<meta itemprop="position" content="2">
				</li>
	 	</ol>
	</nav>
	<header>
		<div class="container text-center">
			<h1 class="m-0"><?php AfficheTitreRubrique( $_GET['id']) ?></h1>
			<?php AfficheTexteRubrique( $_GET['id']) ?>
		</div>
	</header>

	<section class="hfeed">
	  <div class="container">

	<?php if( !empty($articles) ) : ?>
		<p class="h3">Les articles de la rubrique</p>
		<?php foreach( $articles as $article ) : the_post() ?>
		  <article id="post-<?php the_id(); ?>" class="hentry clearfix" itemscope itemtype="http://schema.org/Recipe">
				<div class="article-media float-left col-4">
					<?php afficheMedias(); ?>
				</div>
				<header class="article-header">
					<h2 itemprop="name" class="hentry-title"><?php the_title() ?></h2>
					<div class="meta">
							<p>Posté le
							<time class="published" itemprop="dateCreated" datetime="<?php the_date_iso() ?>"><?php the_date() ?></time></p>
					</div>
					<p><?php afficheTags(); ?></p>
				</header>
				<div class="entry-summary" itemprop="articleBody">
					<?php echo cutContent( get_the_content( get_the_id() ) , 50 ) ?>
				</div>
				<a class="btn" href="<?php the_url() ?>" itemprop="url" rel="bookmark">Lire la suite</a>
		  </article>

		<?php endforeach; ?>

	<?php else : ?>
			<p>Il n'y a pas d'articles à afficher</p>
	<?php endif; ?>

		</div>
	</section>
</main>
<?php include('include/footer.php') ?>
