<?php include('include/header.php') ?>
<main itemprop="mainContentOfPage" class="site-main">

	<header>
		<div class="container">
			<h1 class="m-0">Tags</h1>
			<p class="m-0">Filtrer les articles par tag</p>
		</div>
	</header>

	<section>
		<div class="container">
			<h2 class="text-center">Filtrage AJAX</h2>

			<?php if( !empty($tags) ) : ?>
				<nav class="menu-tag" id="ajax-nav">

					<?php if( !isset( $_GET['tag']) ) : // $_GET['tag'] n'existe pas ?>
					<a class="tag active" href="tags.php">Tous</a>
					<?php else : ?>
					<a class="tag" href="tags.php">Tous</a>
					<?php endif;?>

					<?php foreach ($tags as $tag) : ?>
						<?php
							/*
								Si il existe $_GET['tag'] et qu'il est égal à l'id du tag
								on ajoute la classe active au lien
							*/
							if( isset( $_GET['tag']) AND $_GET['tag'] == $tag['id'] ) : ?>
						<a
							class="tag active"
							href="tags.php?tag=<?php echo $tag['id'] ?>">
							<?php echo strip_tags( $tag['titre'] ) ?>
						</a>
						<?php else : ?>
							<a
								class="tag"
								data-ajax="<?php echo $tag['id'] ?>"
								href="tags.php?tag=<?php echo $tag['id'] ?>">
								<?php echo strip_tags( $tag['titre'] ) ?>
							</a>
						<?php endif;?>
					<?php endforeach; ?>
				</nav>
			<?php endif; ?>

			<div id="ajax-container" class="bg-container">

				<?php include('include/articles-ajax-tag.php') ?>

			</div> <!-- FIN bg-container -->
		</div>
	</section>

	<section class="hfeed p-0">
		<div class="container">
			<h2 class="text-center">Isotope</h2>
			<?php if( !empty($tags) ) : ?>
				<nav class="menu-tag" id="isotope-nav">
					<a class="tag active" href="#">Tous</a>
					<?php foreach ($tags as $tag) : ?>
						<a
							class="tag"
							href="#"
							data-isotope="<?php echo strip_tags( $tag['titre'] ) ?>">
							<?php echo strip_tags( $tag['titre'] ) ?>
						</a>
					<?php endforeach; ?>
				</nav>
			<?php endif; ?>
			<div id="isotope-container" class="bg-container">
				<?php include('include/articles-ajax-tag.php') ?>
			</div> <!-- FIN bg-container -->
		</div><!-- FIN container -->
	</section>

</main>
<?php include('include/footer.php') ?>
