'use strict';

$(function()
{
  var isotopeNav,
      isotopeLinkActive,
      isotopeDataAttribut;

  isotopeNav = '#isotope-nav';
  isotopeLinkActive = '#isotope-nav .active';
  isotopeDataAttribut = 'isotope';

  function onClickExecute(e)
  {
    e.preventDefault();
    var tag;
    tag = $(this).data(isotopeDataAttribut);

    $(isotopeLinkActive).removeClass('active');
    $(this).addClass('active');

    if( typeof(tag) == 'undefined' )
    {
      $('#isotope-container article').show('slow');
      return;
    }

    $('#isotope-container article').each( function () {

      if( $(this).hasClass(tag) )
      {
        $(this).show('slow');
      }
      else
      {
        $(this).hide('slow');
      }

    });

  }

  $(isotopeNav).on('click', 'a' ,onClickExecute);

});
