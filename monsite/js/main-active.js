'use strict';

document.addEventListener('DOMContentLoaded', function()
{


 document.querySelector('html').classList.add('js');



  const url = window.location.pathname; // L'url de la page
  const sections = document.querySelectorAll('.split-container');
  const buttons = document.querySelectorAll('.call-action');
//  const  intElemScrollTop = element.scrollTop;
  const headerFixed = document.getElementById('banner');
  const headerFixedHeight = headerFixed.offsetHeight;


  window.addEventListener('scroll', function() {

    sections.forEach( function( section )
    {
      const hrefNavLink = '#' + section.getAttribute('id'); // id="toto"

      if( window.scrollY >= section.offsetTop - headerFixedHeight )
    //  if(window.section.scrollTop > 350 || document.documentElement.scrollTop > 350)
      {
        //Supprimer la classe active du lien précédent
        document.querySelector('.active').classList.remove('active');

        //Ajouter la classe active au lien de la section : celui dont l'attribut href = # + id de la section en cours
        document.querySelector('[href="' + hrefNavLink + '"]').classList.add('active');
      }

    });

  });




  buttons.forEach( function( button ) {

    button.addEventListener('click', function(event) {

      event.preventDefault();

      const anchorCible = this.getAttribute('href'); // href="#toto" est le lien allant sur id="toto"
      const cible = document.querySelector( anchorCible  ); // querySelector('#toto') cible id="toto"

      //Scroller jusqu'à la section ciblée : obligatoire si pas de split
      window.scroll(0, cible.offsetTop - headerFixedHeight);

      //Supprimer la classe anime-slide-in à la section précédente
      document.querySelector('.anime-slide-in').classList.remove('anime-slide-in');

      //Supprimer la classe active du lien précédent
      document.querySelector('.active').classList.remove('active');

      //Ajouter la classe active au lien de la navigation correspondant (this ne suffit pas ->  on peut cliquer aussi sur les les flèches
      document.querySelector('#menu [href="' + anchorCible + '"]').classList.add('active');

      // Ajouter la classe anime-slide-in à la section ciblée
      cible.classList.add('anime-slide-in');

      /*
        Insertion d'une entrée dans l'historique du navigateur
        - L'url de la page prend la forme donnée
        - Lors de la navigation dans l'historique, l'ancre de la section ciblée sera récupérée
        afin de reproduire ces actions
      */
      window.history.pushState({section : anchorCible},'', url + anchorCible);
    });

  });


  window.onpopstate = function(event) {

    if( event.state !== null )
    {
      const cible = document.querySelector( event.state.section  );
      document.querySelector('.anime-slide-in').classList.remove('anime-slide-in');
      cible.classList.add('anime-slide-in');
    }
  };

});









/*
replace   calcul = calcul.replace(/x/g, '*');
this
inner.HTML

*/

	var touches = document.querySelectorAll('span');
	var operateurs = ['/', 'x', '-', '+'];

for(var i = 0; i < touches.length; i++) {
	touches[i].onclick = function(sourie) {

    console.log(touches);
		var ecran = document.querySelector('.ecran');
	  var boutonEcran = ecran.innerHTML;
	  var boutonNombre = this.innerHTML;

		console.log(boutonNombre);




		if(boutonNombre === 'AC')
		{
			ecran.innerHTML = '';
		}

		else	if(boutonNombre === '=') {

			var calcul = boutonEcran;
			var dernierCaractere = boutonEcran;
			calcul = calcul.replace(/x/g, '*');


		}


	}
}




















var touches = document.querySelectorAll('#calculette span');
var operateurs = ['/', 'x', '-', '+'];
var decimaleAjoute = false;

for(var i = 0; i < touches.length; i++) {
	touches[i].onclick = function(e) {

		var ecran = document.querySelector('.ecran');
		var valeurEcran = ecran.innerHTML;
		var valeurBouton = this.innerHTML;

		if(valeurBouton == 'AC') {
			ecran.innerHTML = '';
			decimaleAjoute = false;
		}
		else	if(valeurBouton == '=')
		{
			var calcul = valeurEcran;
			var dernierCaractere = valeurEcran[valeurEcran.length - 1];
			calcul = calcul.replace(/x/g, '*');

			if(operateurs.indexOf(dernierCaractere) > -1 || dernierCaractere == '.')
			{
				calcul = calcul.replace(/.$/, '');
			}
			if(calcul)
			{
				ecran.innerHTML = eval(calcul);
			}
			decimaleAjoute = false;

		} else if (operateurs.indexOf(valeurBouton) > -1)
		 {
			var dernierCaractere = valeurEcran[valeurEcran.length - 1];
			if (valeurEcran == '-' && valeurBouton == '+')
			{
				ecran.innerHTML = '';
			}
			else if (valeurEcran == ''
			&& valeurBouton == '-')
			{
				ecran.innerHTML = valeurBouton;
			}
			else if (valeurEcran != '' && operateurs.indexOf(dernierCaractere) == -1)
			{
				ecran.innerHTML += valeurBouton;
			}

			if (operateurs.indexOf(dernierCaractere) > -1 && valeurEcran.length > 1)
			{
				ecran.innerHTML = valeurEcran.replace(/.$/, valeurBouton);
			}
			decimaleAjoute =false;
		}
		 else if (valeurBouton == '.')
		 {
			if(!decimaleAjoute) {
				ecran.innerHTML += valeurBouton;
				decimaleAjoute = true;
			}
		} else
		{
			ecran.innerHTML += valeurBouton;
		}
		e.preventDefault();
	}
}
